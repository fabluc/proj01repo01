#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2_$3.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
Opt="$2"
IpVer="$3"

source inc.sh

PhyItf="eth0"

fEnvGetRemote

fRemoteFwallUpdate()
{
	fRemoteCmd "ip6tables -A FORWARD -i bridge0 -p udp --dport 53 -o $WwanMdcItf -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o bridge0 -p ipv6-icmp -m icmp6 --icmpv6-type 131 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o eth0 -p ipv6-icmp -m icmp6 --icmpv6-type 133 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanMdcItf -p tcp --dport 20 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanMdcItf -p tcp --dport 21 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanMdcItf -p tcp --dport 80 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanMdcItf -p udp --dport 53 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanMdcItf -p udp --dport 80 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp --dport 20 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp --dport 21 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp --dport 80 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p udp --dport 53 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p udp --dport 80 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 123 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 132 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 134 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 135 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 136 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 137 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 143 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 1 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i bridge0 -p udp --dport 53 -o $WwanMdcItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i bridge0 -p udp --dport 53 -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i bridge0 -p udp -o $WwanMdcItf -m physdev --physdev-in eth0 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i bridge0 -p udp -o $WwanQcmapItf -m physdev --physdev-in eth0 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanMdcItf -p udp --sport 53 -o bridge0 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -p udp --sport 53 -o bridge0 -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp --sport 137 --dport 137 -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp --sport 138 --dport 138 -j DROP"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p icmp -m icmp --icmp-type 0 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p udp -m udp --sport 53 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanMdcItf -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanMdcItf -p tcp --dport 20 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanMdcItf -p tcp --dport 21 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanMdcItf -p tcp --dport 80 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanMdcItf -p udp --dport 53 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp --dport 20 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp --dport 21 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp --dport 80 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p udp --dport 53 -j ACCEPT"

	fRemoteFwallLog "iptables" "n"
	fRemoteFwallLog "ip6tables" "n"
}

fIcmp()
{
	case "$Opt" in
		qcmap)
			fIcmpRemoteExt "$IpVer" "$1" "$WwanQcmapItf"
			;;
		mdc)
			fIcmpRemoteExt "$IpVer" "$1" "$WwanMdcItf"
			;;
	esac
}
fCurlHttpDl()
{
	case "$Opt" in
		qcmap)
			fCurlRemoteHttpDlExt "$IpVer" "$1" "$WwanQcmapItf"
			;;
		mdc)
			fCurlRemoteHttpDlExt "$IpVer" "$1" "$WwanMdcItf"
			;;
	esac
}
fCurlFtpDl()
{
	case "$Opt" in
		qcmap)
			fCurlRemoteFtpDlExt "$IpVer" "$1" "$WwanQcmapItf"
			;;
		mdc)
			fCurlRemoteFtpDlExt "$IpVer" "$1" "$WwanMdcItf"
			;;
	esac
}

case "$Action" in
	on)
		fRemoteFwallReset

		fRemoteConnectivityPrepare

		fRemoteLan "check" "$PhyItf"

		fRemoteWwan "on" "$Opt"
		fRemoteWwan "check" "$Opt"

		fRemoteFwallUpdate
		;;
	status)
		fRemoteWwan "$Action" "$Opt"
		;;
	off)
		fRemoteWwan "off" "$Opt"

		fRemoteFwallReset
		;;
	play)
		fRemoteLan "check" "$PhyItf"

		fRemoteWwan "check" "$Opt"

		case "$IpVer" in
			4)
				case "$Opt" in
					qcmap)
						fIcmp "$DnsSrvQcmapIpv4Addr"
						;;
					mdc)
						fIcmp "$DnsSrvMdcIpv4Addr"
						;;
				esac

				fIcmp "8.8.8.8"
				fIcmp "www.google.fr"
				fCurlHttpDl "http://www.google.fr"

				fIcmp "www.yahoo.fr"
				fCurlHttpDl "http://www.yahoo.fr"

				fIcmp "ftp.sjtu.edu.cn"
				fCurlFtpDl "ftp://ftp.sjtu.edu.cn/pub/README.NetInstall"
				;;
			6)
				case "$Opt" in
					qcmap)
						fIcmp "$DnsSrvQcmapIpv6Addr"
						;;
					mdc)
						fIcmp "$DnsSrvMdcIpv6Addr"
						;;
				esac

				fIcmp "2001:4860:4860::8888"
				fIcmp "www.google.fr"
				fCurlHttpDl "http://www.google.fr"

				fIcmp "ftp6.sjtu.edu.cn"
				fCurlFtpDl "ftp://ftp6.sjtu.edu.cn/pub/README.NetInstall"

				if false; then
					# 2 packets transmitted, 0 packets received, 100% packet loss
					fIcmp "$ChinaTelecomGuangdongIpv6Addr"

					# 2 packets transmitted, 0 packets received, 100% packet loss
					fIcmp "ipv6.baidu.com"

					fCurlHttpDl "http://ipv6.baidu.com/img/bd_logo1.png"
				fi
				;;
		esac
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
