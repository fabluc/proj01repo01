#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
Opt="$2"

source inc.sh

CurrDir=$(pwd)
LogsDir=$(printf "%s/logs" "$CurrDir")

fDmLogChk()
{
	local LogLatest

	LogLatest=$(ls -t $LogsDir/$StampToday*.qmdl | head -1)

	grep -qa "SWI\ QCMAP" $LogLatest
	if [[ $? -ne 0 ]]; then
		printf "${DisplWrn}%s NOK${DisplNormal}\n" "$LogLatest"
	else
		printf "${DisplOk}%s OK (%s)${DisplNormal}\n" "$LogLatest" "$FUNCNAME:$LINENO"
	fi
}

case "$Action" in
	ipa|iprem)
		fEnvGetRemote
		;;
	iploc)
		fEnvGetLocal
		;;
	dmip2loc|dmusb2loc|dmusb2remote)
		fEnvGetLocal
		fEnvGetRemote
		;;
	*)
		;;
esac

case "$Action" in
	ipa)
		tKerDynDbgFiles=( \
		"ipa_api.c" \
		"ipa.c" \
		"ipa_client.c" \
		"ipa_dp.c" \
		"ipa_flt.c" \
		"ipa_hdr.c" \
		"ipa_intf.c" \
		"ipa_nat.c" \
		"ipa_qmi_service.c" \
		"ipa_rm.c" \
		"ipa_rm_dependency_graph.c" \
		"ipa_rm_resource.c" \
		"ipa_rt.c" \
		"ipa_uc.c" \
		"ipa_utils.c" \
		"rmnet_ipa.c" \
		"teth_bridge.c" \
		)

		case "$Opt" in
			on)
				fRemoteIpaStatsGet

				fRemoteCmd "klogd -c 8"

				for Idx in ${!tKerDynDbgFiles[*]}; do
					KerDynDbgFile=${tKerDynDbgFiles[$Idx]}
					fRemoteCmd "echo file $KerDynDbgFile +p > /sys/kernel/debug/dynamic_debug/control"
				done

				fRemoteFwallMon
				;;
			off)
				for Idx in ${!tKerDynDbgFiles[*]}; do
					KerDynDbgFile=${tKerDynDbgFiles[$Idx]}
					fRemoteCmd "echo file $KerDynDbgFile -p > /sys/kernel/debug/dynamic_debug/control"
				done

				fRemoteCmd "klogd -c 7"

				fRemoteIpaStatsGet
				;;
			mon)
				fRemoteCmd "watch -d -n 1 cat /sys/kernel/debug/ipa/ntn"
		esac
		;;
	iploc)
		sudo pkill tcpdump

		Log=$(printf "%s/%s" "$LogsDir" "$Stamp")
		Log+=$(printf "%s" ".ubuntu.pcap")
		case "$Opt" in
			on)
				sudo tcpdump -i any -tttt -vvv -n -e -w "$Log"
				;;
			off)
				LogLatest=$(ls -t $LogsDir/$StampToday*.pcap | head -1)
				;;
		esac
		;;
	iprem)
		fRemoteCmd "pkill tcpdump"

		Log=$(printf "%s" "$Stamp")
		Log+=$(printf "%s" ".ar.pcap")
		case "$Opt" in
			on)
				fRemoteCmd "rm /tmp/*.pcap"
				fRemoteCmd "tcpdump -i any -tttt -vvv -n -e -w /tmp/$Log"
				;;
			off)
				Files=$(adb shell ls /tmp/*.pcap)
				Files=${Files%$'\r'}
				for Idx in ${!Files[*]}; do
					File=${Files[$Idx]}
					adb pull "$File" "$LogsDir/"
					FileBasename=$(basename "$File")
					#sudo wireshark "$LogsDir/$FileBasename"
				done
				LogLatest=$(ls -t $LogsDir/$StampToday*.pcap | head -1)
				;;
		esac
		;;
	dmip2loc)
		sudo pkill dm-logger

		Log=$(printf "%s/%s" "$LogsDir" "$Stamp")
		Log+=$(printf "%s" ".qmdl")
		case "$Opt" in
			on)
				cd "$HOME/Documents/Debug/dm-logger"
				sudo python dm-logger.py 5555 "$HOME/Documents/Debug/linux_data.sqf" -o "$Log" --maxfilesize=10M --maxlogsize=20000M --mergesize=1000M
				;;
			off)
				fDmLogChk
				;;
		esac
		;;
	dmusb2loc)
		Log=$(printf "%s/%s" "$LogsDir" "$Stamp")
		Log+=$(printf "%s" ".qmdl")
		case "$Opt" in
			on)
				sudo pkill dmcapture

				if [ ! -c "$ArUsbDmDev" ]; then
					printf "${DisplErr}%s not found${DisplNormal}\n" "$LINENO" "$ArUsbDmDev"
				else
					cd "$DmCaptDir"
					sudo ./dmcapture.sh -l -d "$ArUsbDmDev" -f "$HOME/Documents/Debug/linux_data.sqf" -o "$Log"
					#python py-dmcapture "$ArUsbDmDev" "$HOME/Documents/Debug/linux_data.sqf"
				fi
				;;
			off)
				sudo pkill dmcapture
				fDmLogChk
				;;
		esac
		;;
	dmusb2remote)
		case "$Opt" in
			on)
				sudo pkill dmcapture

				if [ ! -c "$ArUsbDmDev" ]; then
					printf "${DisplErr}%s not found${DisplNormal}\n" "$LINENO" "$ArUsbDmDev"
				else
					cd "$DmCaptDir"
					sudo ./dmcapture.sh -r 10.40.45.8 -p 50814 -d "$ArUsbDmDev"
				fi
				;;
			off)
				sudo pkill dmcapture
				;;
		esac
		;;
	*)
		;;
esac

cd "$CurrDir"

} 2>&1 | tee "$LogName"
