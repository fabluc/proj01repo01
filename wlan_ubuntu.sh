#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2_$3.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
Opt="$2"
IpVer="$3"

source inc.sh

fEnvGetLocal

PhyItf="wlp3s0"

fIcmp()
{
	fIcmpExt "$IpVer" "$1" "$PhyItf"
}
fCurlHttpDl()
{
	fCurlHttpDlExt "$IpVer" "$1" "$PhyItf"
}
fCurlHttpUl()
{
	fCurlHttpUlExt "$IpVer" "$1" "$PhyItf"
}
fCurlFtpDl()
{
	fCurlFtpDlExt "$IpVer" "$1" "$PhyItf"
}

case "$Action" in
	dhcp)
		fDhcpRenew "$PhyItf"
		;;
	on)
		sudo service network-manager start

		fLocalLan "check" "$PhyItf"

		#sudo ifconfig "$PhyItf" mtu $Mtu
		;;
	off)
		;;
	play)
		fLocalLan "check" "$PhyItf"

		case "$IpVer" in
			4)
				fIcmp "8.8.8.8"
				fIcmp "www.google.fr"
				fCurlHttpDl "http://www.google.fr"

				fIcmp "www.yahoo.fr"
				fCurlHttpDl "http://www.yahoo.fr"

				fIcmp "ftp.sjtu.edu.cn"
				fCurlFtpDl "ftp://ftp.sjtu.edu.cn/pub/README.NetInstall"
				;;
			6)
				fIcmp "$ChinaTelecomGuangdongIpv6Addr"
				fCurlHttpDl "http://[$ChinaUnicomGuangdongIpv6Addr]/img/bd_logo1.png"

				fIcmp "ipv6.baidu.com"
				fCurlHttpDl "http://ipv6.baidu.com/img/bd_logo1.png"

				fIcmp "2001:4860:4860::8888"
				fIcmp "www.google.fr"
				fCurlHttpDl "http://www.google.fr"

				fIcmp "2001:a18:1:20::42"
				fIcmp "ipv6forum.com"
				fCurlHttpDl "http://ipv6forum.com"

				fIcmp "ftp2.no.netbsd.org"
				fCurlFtpDl "ftp://ftp2.no.netbsd.org/robots.txt"

				fIcmp "ftp6.sjtu.edu.cn"
				fCurlFtpDl "ftp://ftp6.sjtu.edu.cn/pub/README.NetInstall"
				;;
		esac
		;;
	stress)
		fLocalLan "check" "$PhyItf"

		case "$Opt" in
			dl)
				case "$IpVer" in
					4)
						fCurlHttpDl "http://ipv4.bouygues.testdebit.info/10G.iso"

						#fCurlHttpDl "https://ipv4.bouygues.testdebit.info:8080/10G.iso"

						fIcmp "ftp.sjtu.edu.cn"
						fCurlFtpDl "ftp://ftp.sjtu.edu.cn/ubuntu-cd/19.10/ubuntu-19.10-desktop-amd64.iso"
						;;
					6)
						fCurlHttpDl "http://ipv6.bouygues.testdebit.info/10G.iso"

						fCurlHttpDl "https://ipv6.bouygues.testdebit.info:8080/10G.iso"

						#ftp://ftp2.no.netbsd.org/ubuntu-iso/19.10/ubuntu-19.10-desktop-amd64.iso

						fIcmp "ftp6.sjtu.edu.cn"
						fCurlFtpDl "ftp://ftp6.sjtu.edu.cn/ubuntu-cd/19.10/ubuntu-19.10-desktop-amd64.iso"
						;;
				esac
				;;
			ul)
				case "$IpVer" in
					4)
						#fCurlHttpUl "http://www.example.com"
						fCurlHttpUl "https://ptsv2.com/t/ak0n5-1586017009"

						#fCurlFtpUl "ftp://ftp.example.com/upload/"

						#fCurlHttpUl "http://bouygues.testdebit.info"
						#fCurlHttpUl "https://bouygues.testdebit.info"
						;;
					6)
						#fCurlHttpUl "http://bouygues.testdebit.info"
						#fCurlHttpUl "https://bouygues.testdebit.info"
						;;
				esac
				;;
		esac
		;;

	*)
		;;
esac

} 2>&1 | tee "$LogName"
