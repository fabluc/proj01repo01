#!/bin/bash
LogName=$(printf "%s/%s_%s_$1.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"

source inc.sh

fEnvGetLocal

case "$Action" in
	cfg)
		fRemoteCmd "uartMode set 2 console; uartMode set 1 app; reboot"
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
