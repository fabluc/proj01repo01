#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2_$3.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
IpVer="$2"

source inc.sh

fEnvGetLocal

PhyItf="enp0s25"
VlanItf="$PhyItf.$VlanId"

fIcmp()
{
	fIcmpExt "$IpVer" "$1" "$VlanItf"
}
fCurlHttpDl()
{
	fCurlHttpDlExt "$IpVer" "$1" "$VlanItf"
}
fCurlFtpDl()
{
	fCurlFtpDlExt "$IpVer" "$1" "$VlanItf"
}

case "$Action" in
	on)
		sudo service network-manager stop
		#sudo service network-manager restart

		sudo modprobe 8021q

		fLocalRpFilter "$PhyItf" "off"

		fLocalIpFwd

		sudo ip addr flush dev "$PhyItf"
		sudo ip li set dev "$PhyItf" up

		fLocalLan "check" "$PhyItf"

		sudo vconfig add "$PhyItf" "$VlanId"
		sudo ip -6 addr add "$VlanUbuntuIpv6Addr" dev "$VlanItf"
		sudo ip -4 addr add "$VlanUbuntuIpv4Addr" dev "$VlanItf"
		sudo ip li set dev "$VlanItf" up
		fLocalLan "check" "$VlanItf"

		sudo ip -4 route add "$VlanUbuntuIpv4Nw2" dev "$VlanItf"
		sudo ip -6 route add "$VlanUbuntuIpv6Nw" dev "$VlanItf"

		sudo route add default gw "$BridgeArIpv4Addr"

		sudo ifconfig "$PhyItf" mtu $Mtu
		sudo ifconfig "$VlanItf" mtu $Mtu
		;;
	off)
		sudo vconfig rem "$VlanItf"

		fLocalRpFilter "$PhyItf" "on"

		#sudo service network-manager start
		;;
	play)
		fLocalLan "check" "$PhyItf"
		if [[ "$Opt" == "vlan" ]]; then
			fLocalLan "check" "$VlanItf"

			fLocalDnsGet "$VlanItf"
		else
			fLocalDnsGet "$PhyItf"
		fi

		case "$IpVer" in
			4)
				fIcmp "$BridgeArIpv4Addr"

				fIcmp "8.8.8.8"
				fIcmp "www.google.fr"

				fCurlHttpDl "http://www.google.fr"
				;;
			6)
				fBridgeArIpv6AddrGet
				fIcmp "$BridgeArIpv6Addr"

				fIcmp "$VlanArIpv6Addr"
				;;
		esac
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
