#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2_$3.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
Opt="$2"
IpVer="$3"

source inc.sh

fEnvGetLocal

PhyItf="enp0s25"

fIcmp()
{
	fIcmpExt "$IpVer" "$1" "$PhyItf"
}
fCurlHttpDl()
{
	fCurlHttpDlExt "$IpVer" "$1" "$PhyItf"
}
fCurlFtpDl()
{
	fCurlHttpDlExt "$IpVer" "$1" "$PhyItf"
}

case "$Action" in
	on)
		sudo service network-manager stop
		#sudo service network-manager restart

		sudo modprobe 8021q

		fLocalRpFilter "$PhyItf" "off"

		fLocalIpFwd

		#sudo ip addr flush dev "$PhyItf"
		sudo ip li set dev "$PhyItf" up

		fLocalLan "check" "$PhyItf"

		sudo route add default gw "$BridgeArIpv4Addr"

		sudo ifconfig "$PhyItf" mtu $Mtu
		;;
	off)
		fLocalRpFilter "$PhyItf" "on"

		#sudo service network-manager start
		;;
	play)
		case "$IpVer" in
			4)
				fLocalLan "check" "$PhyItf"

				fLocalDnsGet "$PhyItf"

				fIcmp "$BridgeArIpv4Addr"

				fIcmp "8.8.8.8"
				fIcmp "www.google.fr"
				fCurlHttpDl "http://www.google.fr"

				fCurlHttpDl "http://www.baidu.com/img/bd_logo1.png"
				;;
			6)
				fLocalLan "check" "$PhyItf"

				fLocalDnsGet "$PhyItf"

				fBridgeArIpv6AddrGet
				fIcmp "$BridgeArIpv6Addr"
				;;
		esac
		;;
	stress)
		fLocalLan "check" "$PhyItf"

		fLocalDnsGet "$L2tpItf"

		case "$Opt" in
			dl)
				case "$IpVer" in
					4)
						fCurlHttpDl "http://ipv4.bouygues.testdebit.info/10G.iso"

						#fCurlHttpDl "https://ipv4.bouygues.testdebit.info:8080/10G.iso"

						fIcmp "ftp.sjtu.edu.cn"
						fCurlFtpDl "ftp://ftp.sjtu.edu.cn/ubuntu-cd/19.10/ubuntu-19.10-desktop-amd64.iso"
						;;
					6)
						fCurlHttpDl "http://ipv6.bouygues.testdebit.info/10G.iso"

						fCurlHttpDl "https://ipv6.bouygues.testdebit.info:8080/10G.iso"

						#ftp://ftp2.no.netbsd.org/ubuntu-iso/19.10/ubuntu-19.10-desktop-amd64.iso

						fIcmp "ftp6.sjtu.edu.cn"
						fCurlFtpDl "ftp://ftp6.sjtu.edu.cn/ubuntu-cd/19.10/ubuntu-19.10-desktop-amd64.iso"
						;;
				esac
				;;
			ul)
				case "$IpVer" in
					4)
						fCurlHttpUl "https://ptsv2.com/t/ak0n5-1586017009"

						fCurlHttpUl "http://bouygues.testdebit.info"
						fCurlHttpUl "https://bouygues.testdebit.info"
						;;
					6)
						fCurlHttpUl "http://bouygues.testdebit.info"
						fCurlHttpUl "https://bouygues.testdebit.info"
						;;
				esac
				;;
		esac
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
