#!/bin/bash
LogName=$(printf "%s/%s_%s_$1.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"

source inc.sh

fEnvGetRemote

PhyItf="eth0"

case "$Action" in
	mon)
		fRemoteFwallMon
		;;
	get)
		fRemoteFwallGet
		;;
	reset)
		fRemoteFwallReset
		;;
	save)
		fRemoteCmd "iptables-save > /tmp/iptables-save.log"
		fRemoteCmd "ip6tables-save > /tmp/ip6tables-save.log"
		fRemoteCmd "ls -alt /tmp/ip*tables*"

		adb pull "/tmp/iptables-save.log" "logs/iptables-save.${Stamp}.log"
		adb pull "/tmp/ip6tables-save.log" "logs/ip6tables-save.${Stamp}.log"
		ls -alt logs/ip*tables*
		;;
	restore)
		adb push iptables4.ok.log /tmp/; fKeyToContinue
		fRemoteCmd "iptables-restore < /tmp/iptables4.ok.log"; fKeyToContinue

		adb push iptables6.ok.log /tmp/; fKeyToContinue
		fRemoteCmd "ip6tables-restore < /tmp/iptables6.ok.log"; fKeyToContinue
		;;
	whatqcmapdoes_vlanl2tpwan)
		# "iptables -P INPUT DROP"
		# "iptables -P FORWARD DROP"

		# "iptables -P OUTPUT ACCEPT"

		# "iptables -A INPUT -i $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_HttpSec -j DROP"
		# "iptables -A INPUT -i $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_Http -j DROP"

		# "iptables -A INPUT -i lo -j ACCEPT"
		# "iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT"
		# "iptables -A INPUT -p udp -m udp --sport 53 -j ACCEPT"
		# "iptables -A INPUT -p icmp -m icmp --icmp-type 0 -j ACCEPT"
		# "iptables -A INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT"
		# "iptables -A INPUT -i eth0 -p tcp -m tcp --dport 22 -j ACCEPT"
		# "iptables -A INPUT -i ecm0 -p tcp -m tcp --dport 22 -j ACCEPT"
		# "iptables -A INPUT -i bridge0 -p tcp -m tcp --dport 22 -j ACCEPT"
		# "iptables -A INPUT -i bridge0 -p udp -m udp --dport 67 -j ACCEPT"
		# "iptables -A INPUT -i bridge0 -p udp -m udp --sport 68 -j ACCEPT"
		# "iptables -A INPUT -p udp -m udp --dport 53 -j ACCEPT"
		# "iptables -A INPUT -p tcp -m tcp --dport 53 -j ACCEPT"
		# "iptables -A FORWARD -i bridge0 -p tcp -m physdev --physdev-out l2tpeth0 -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1860"
		# "iptables -A FORWARD -i bridge0 -p tcp -m physdev --physdev-in l2tpeth0 -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1860"
		# "iptables -A FORWARD -p icmp -m icmp --icmp-type 0 -j ACCEPT"
		# "iptables -A FORWARD -p icmp -m icmp --icmp-type 8 -j ACCEPT"
		# "iptables -A FORWARD -p tcp -m tcp --dport $ip_Port_HttpSec -j ACCEPT"
		# "iptables -A FORWARD -p tcp -m tcp --sport $ip_Port_HttpSec -j ACCEPT"
		# "iptables -A FORWARD -p tcp -m tcp --dport $ip_Port_Http -j ACCEPT"
		# "iptables -A FORWARD -p tcp -m tcp --sport $ip_Port_Http -j ACCEPT"

		# "iptables -A FORWARD -i bridge0 -p tcp -m state --state INVALID -j DROP"
		# "iptables -A OUTPUT -o rmnet_data7 -p udp -m udp --dport 1900 -m comment --comment \"Drop SSDP on WWAN\" -j DROP"
		# "iptables -A OUTPUT -o rmnet_data6 -p udp -m udp --dport 1900 -m comment --comment \"Drop SSDP on WWAN\" -j DROP"
		# "iptables -A OUTPUT -o rmnet_data5 -p udp -m udp --dport 1900 -m comment --comment \"Drop SSDP on WWAN\" -j DROP"
		# "iptables -A OUTPUT -o rmnet_data4 -p udp -m udp --dport 1900 -m comment --comment \"Drop SSDP on WWAN\" -j DROP"
		# "iptables -A OUTPUT -o rmnet_data3 -p udp -m udp --dport 1900 -m comment --comment \"Drop SSDP on WWAN\" -j DROP"
		# "iptables -A OUTPUT -o rmnet_data2 -p udp -m udp --dport 1900 -m comment --comment \"Drop SSDP on WWAN\" -j DROP"
		# "iptables -A OUTPUT -o rmnet_data1 -p udp -m udp --dport 1900 -m comment --comment \"Drop SSDP on WWAN\" -j DROP"
		# "iptables -A OUTPUT -o rmnet_data0 -p udp -m udp --dport 1900 -m comment --comment \"Drop SSDP on WWAN\" -j DROP"

		# "ip6tables -P INPUT DROP"
		# "ip6tables -P FORWARD DROP"

		# "ip6tables -P OUTPUT ACCEPT"
		# "ip6tables -A INPUT -i lo -j ACCEPT"
		# "ip6tables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT"
		# "ip6tables -A INPUT -p udp -m udp --sport 53 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 1 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 2 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 3 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 4 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 133 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 134 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 135 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 136 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 137 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 141 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 142 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 148 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 149 -j ACCEPT"
		# "ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 130 -j ACCEPT"
		# "ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 131 -j ACCEPT"
		# "ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 132 -j ACCEPT"
		# "ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 143 -j ACCEPT"
		# "ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 151 -j ACCEPT"
		# "ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 152 -j ACCEPT"
		# "ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 153 -j ACCEPT"
		# "ip6tables -A INPUT -i eth0 -p tcp -m tcp --dport 22 -j ACCEPT"
		# "ip6tables -A INPUT -i ecm0 -p tcp -m tcp --dport 22 -j ACCEPT"
		# "ip6tables -A INPUT -i bridge0 -p tcp -m tcp --dport 22 -j ACCEPT"
		# "ip6tables -A INPUT -i bridge0 -p udp -m udp --dport 67 -j ACCEPT"
		# "ip6tables -A INPUT -i bridge0 -p udp -m udp --sport 68 -j ACCEPT"
		# "ip6tables -A INPUT -p udp -m udp --dport 53 -j ACCEPT"
		# "ip6tables -A INPUT -p tcp -m tcp --dport 53 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -j ACCEPT"
		# "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 129 -j ACCEPT"
		# "ip6tables -A FORWARD -i bridge0 -p tcp -m physdev --physdev-out l2tpeth0 -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1840"
		# "ip6tables -A FORWARD -i bridge0 -p tcp -m physdev --physdev-in l2tpeth0 -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1840"
		# "ip6tables -A FORWARD -p tcp -m tcp --dport $ip_Port_HttpSec -j ACCEPT"
		# "ip6tables -A FORWARD -p tcp -m tcp --sport $ip_Port_HttpSec -j ACCEPT"
		# "ip6tables -A FORWARD -p tcp -m tcp --dport $ip_Port_Http -j ACCEPT"
		# "ip6tables -A FORWARD -p tcp -m tcp --sport $ip_Port_Http -j ACCEPT"
		# "ip6tables -A FORWARD -i bridge0 -o bridge0 -j ACCEPT"
		# "ip6tables -A FORWARD -i bridge0 -o ppp0 -j ACCEPT"
		# "ip6tables -A FORWARD -i ppp0 -o bridge0 -j ACCEPT"

		# "ip6tables -A FORWARD -i bridge0 -j DROP"
		# "ip6tables -A FORWARD -i ppp0 -j DROP"
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
