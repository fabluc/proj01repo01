#!/bin/bash
LogName=$(printf "%s/%s_%s_$1.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"

source inc.sh

fEnvGetRemote

PhyItf="eth0"

fRemoteFwallReset

case "$Action" in
	on)
		fRemoteLan "on"
		fRemoteLan "check" "$PhyItf"
		;;
	off)
		fRemoteLan "off" "$PhyItf"
		;;
	onoffloop)
		while true; do
			fRemoteLan "check" "$PhyItf"
			fRemoteLan "off" "$PhyItf"
			fRemoteLan "on"
		done
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
