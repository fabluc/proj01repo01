#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2_$3_$4.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
Opt="$2"
IpVer="$3"
Opt2="$4"

source inc.sh

fEnvGetRemote

PhyItf="eth0"

fRemoteFwallUpdate()
{
	fRemoteCmd "ip6tables -A FORWARD -i eth0 -p tcp --dport $ip_Port_Http -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i eth0 -p tcp --sport $ip_Port_Http -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i $WwanQcmapItf -o bridge0 -p tcp -m tcp --sport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i $WwanQcmapItf -o bridge0 -p tcp --sport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i $WwanQcmapItf -o bridge0 -p udp -m udp --sport $ip_Port_Ntp -j DROP"
	fRemoteCmd "ip6tables -A FORWARD -i $WwanQcmapItf -o $WwanQcmapItf -p tcp -m tcp --sport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i $WwanQcmapItf -o $WwanQcmapItf -p udp -m udp --sport $ip_Port_Ntp -j DROP"
	fRemoteCmd "ip6tables -A FORWARD -p icmpv6 --icmpv6-type echo-reply -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p icmpv6 --icmpv6-type echo-request -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p ipv6-icmp -m icmp6 --icmpv6-type 1 -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p ipv6-icmp -m icmp6 --icmpv6-type 3 -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p ipv6-icmp -m icmp6 --icmpv6-type echo-reply -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p ipv6-icmp -m icmp6 --icmpv6-type echo-request -j ACCEPT"
	fRemoteCmd "ip6tables -A INPUT -i $WwanQcmapItf -p tcp -m tcp --sport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 135 -j ACCEPT"
	fRemoteCmd "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 136 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_Http -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p udp --sport 546 --dport 547 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 129 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 131 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 132 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 133 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 134 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 135 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 136 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 137 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 143 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 1 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i bridge0 -o $WwanQcmapItf -p udp --dport $ip_Port_Ntp -o $WwanQcmapItf -m physdev --physdev-in $PhyItf -j DROP"
	fRemoteCmd "iptables -A FORWARD -i eth0 -o $WwanQcmapItf -p icmp -m icmp --icmp-type 3 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -o $WwanQcmapItf -p tcp --sport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -p tcp --dport 53 -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -p udp --dport 53 -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -p udp --dport $ip_Port_Ntp -o $WwanQcmapItf -j DROP"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -o bridge0 -p tcp -m tcp --sport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -o bridge0 -p tcp --sport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -p udp --sport 53 -o bridge0 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -p udp --sport $ip_Port_Ntp -o bridge0 -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p tcp -m tcp --dport $ip_Port_Http -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p tcp -m tcp --dport $ip_Port_HttpSec -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p tcp -m tcp --dport $ip_Port_Iperf -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp --dport 3289 -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp -m udp --dport $ip_Port_Iperf -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp -m udp --sport 67 --dport 68 -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp --sport 137 --dport 137 -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp --sport 138 --dport 138 -j DROP"
	fRemoteCmd "iptables -A INPUT -i $WwanQcmapItf -p icmp -m icmp --icmp-type 0 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p icmp -m icmp --icmp-type 0 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p icmp -m icmp --icmp-type 11 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p icmp -m icmp --icmp-type 3 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p tcp -m tcp --dport $ip_Port_Iperf -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p tcp -m tcp --sport $ip_Port_Iperf -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p udp -m udp --dport $ip_Port_Iperf -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p udp -m udp --sport $ip_Port_Iperf -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o lo -p icmp -m icmp --icmp-type 3 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o lo -p tcp -m tcp --sport $ip_Port_Iperf -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o lo -p udp --dport 53 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o lo -p udp -m udp --sport $ip_Port_Iperf -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_Http -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_HttpSec -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p udp --dport 53 -j ACCEPT"

	fRemoteFwallLog "iptables" "n"
	fRemoteFwallLog "ip6tables" "n"
}

fIpPerfs3()
{
	local Params="$1"
	local Log="$2"
	local Port="$3"
	local Action="$4"
	local Cmd=""
	local CurrDir
	local LogsDir
	local Files

	CurrDir=$(pwd)
	LogsDir=$(printf "%s/logs" "$CurrDir")

	case "$Action" in
		on)
			fRemoteCmd "rm /tmp/*.iperf.log"

			Cmd="iperf3"
			if [ ! -z "$Params" ]; then
				Cmd+=$(printf " %s" "$Params")
			fi
			if [ ! -z "$Log" ]; then
				Cmd+=$(printf " --logfile %s/%s" "/tmp" "$Log")
			fi
			if [ ! -z "$Port" ]; then
				Cmd+=$(printf " --port %s" "$Port")
			fi
			Cmd+=" --verbose"
			#Cmd+=" --debug"

			fRemoteCmd "$Cmd"
			;;
		off)
			fRemoteCmd "pkill iperf3"

			Files=$(adb shell ls /tmp/*.iperf.log)
			Files=${Files%$'\r'}
			for Idx in ${!Files[*]}; do
				File=${Files[$Idx]}
				adb pull "$File" "$LogsDir/"
				FileBasename=$(basename "$File")
				less -R "$LogsDir/$FileBasename"; fKeyToContinue
			done
			#ls -alt $LogsDir/*.iperf.log

			case "$Proto" in
				udp)
					fRemoteCmd "netstat -u -e"
					;;
				tcp)
					fRemoteCmd "netstat -t -e"
					;;
			esac
			;;
	esac
}

fIpPerfs3Srv()
{
	local Proto="$1"
	local Port="$2"
	local IpVer="$3"
	local Action="$4"
	local Params=""
	local Log

	case "$Action" in
		on)
			fRemoteFwallReset
			fRemoteFwallUpdate
			;;
	esac

	LogSuffix="iperf.log"
	Log=$(printf "%s" "$Stamp")
	Log+=$(printf "_srv_%s_%s_%s" "$IpVer" "$Proto" "$Port")
	Log+=$(printf "%s" ".$LogSuffix")

	Params+=" --server"
	Params+=" -$IpVer"

	# Disabled to allow preliminary nc
	#Params+=" --one-off"

	fIpPerfs3 "$Params" "$Log" "$Port" "$Action"

	case "$Action" in
		off)
			fRemoteFwallReset
			;;
	esac
}

fIpPerfs3Cli()
{
	local Proto="$1"
	local Srv="$2"
	local Port="$3"
	local IpVer="$4"
	local Action="$5"
	local Params=""
	local Log
	local Cmd

	case "$Action" in
		on)
			fRemoteFwallReset
			fRemoteFwallUpdate

			# Does not exit
			#Cmd=$(printf "nc %s %s" "$Srv" "$Port")
			#fRemoteCmd "$Cmd"
			;;
	esac

	LogSuffix="iperf.log"
	Log=$(printf "%s" "$Stamp")
	Log+=$(printf "_cli_%s_%s_%s:%s" "$IpVer" "$Proto" "$Srv" "$Port")
	Log+=$(printf "%s" ".$LogSuffix")

	if [ -z "$Srv" ]; then
		Srv="localhost"
	fi
	Params+=" --client $Srv"

	#Params+=" --reverse"

	#Params+=" --bandwidth 2000m"
	Params+=" --bandwidth 1G"
	Params+=" --time 10"
	Params+=" -$IpVer"

	if [ "$Proto" == "udp" ]; then
		Params+=" --udp"
	fi

	fIpPerfs3 "$Params" "$Log" "$Port" "$Action"

	case "$Action" in
		off)
			fRemoteFwallReset
			;;
	esac
}

CurrDir=$(pwd)
LogsDir=$(printf "%s/logs" "$CurrDir")

case "$Action" in
	iperf3)
		case "$Opt" in
			srv)
				fIpPerfs3Srv "$IpPerfsProto" "$ip_Port_Iperf" "$IpVer" "$Opt2"
				;;
			cli)
				case "$IpVer" in
					4)
						#Srv="localhost"; Port="$ip_Port_Iperf"
						#Srv="bouygues.iperf.fr"; Port="9222"
						#Srv="ping.online.net"; Port="5209"
						#Srv="ping-90ms.online.net"; Port="5209"
						#Srv="localhost"; Port="$ip_Port_Iperf"

						fUbuntuIpv4AddrGet "$PhyItf"
						Srv="$UbuntuIpv4Addr"; Port="$ip_Port_Iperf"

						if false; then
							# iperf3: error - unable to connect to server: Connection timed out
							#Srv="iperf.eenet.ee"; Port="5201"
							#Srv="speedtest.serverius.net"; Port="5002"
							Srv="iperf.volia.net"; Port="5201"
							#Srv="iperf.it-north.net"; Port="5209"
							#Srv="iperf.biznetnetworks.com"; Port="5203"
							#Srv="iperf.scottlinux.com"; Port="5201"
							#Srv="iperf.he.net"; Port="5201"
						fi
						;;
					6)
						#Srv="localhost"; Port="$ip_Port_Iperf"
						#Srv="bouygues.iperf.fr"; Port="9222"
						#Srv="ping6.online.net"; Port="5209"
						#Srv="ping6-90ms.online.net"; Port="5209"
						#Srv="speedtest.serverius.net"; Port="5002"
						#Srv="iperf.biznetnetworks.com"; Port="5203"
						#Srv="iperf.scottlinux.com"; Port="5201"
						#Srv="iperf.he.net"; Port="5201"

						fUbuntuIpv6AddrGet "$PhyItf"
						Srv="$UbuntuIpv6Addr"; Port="$ip_Port_Iperf"
						;;
				esac

				fIpPerfs3Cli "$IpPerfsProto" "$Srv" "$Port" "$IpVer" "$Opt2"
				;;
		esac
		;;
	cpu)
		LogSuffix="ar_top.log"
		Log=$(printf "%s" "$Stamp")
		Log+=$(printf "%s" ".$LogSuffix")
		case "$Opt" in
			stress)
				while true; do
					fRemoteCmd "openssl genrsa -out /dev/null 4096"
				done
				;;
			mon)
				fRemoteCmd "top -d 1 | grep Cpu"
				;;
			monfull)
				fRemoteCmd "top -d 1"
				;;
			logon)
				fRemoteCmd "pkill top"
				fRemoteCmd "rm /tmp/*.$LogSuffix*"

				adb shell "source /etc/profile; top -b -d 1" > "$LogsDir/$Log"
				;;
			logoff)
				Files=$(find "$LogsDir" -name "*.$LogSuffix" -type f)
				Files=${Files%$}
				for File in $Files; do
					FileBasename=$(basename "$File")
					grep --color=never "Cpu" "$LogsDir/$FileBasename"
					fKeyToContinue
				done
				;;
			anal)
				grep -v "top\|Mem\|Cpu\|Swap\|PID" "$Opt2" | dos2unix > $Opt2.$Opt.log
				;;
		esac
		;;
	*)
		;;
esac

cd "$CurrDir"

} 2>&1 | tee "$LogName"
