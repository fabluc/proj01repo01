#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2_$3.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"

source inc.sh

fEnvGetLocal

case "$Action" in
	log)
		fLocalFwallLog "iptables"
		fLocalFwallLog "ip6tables"
		;;
	get)
		sudo iptables -S
		sudo ip6tables -S
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
