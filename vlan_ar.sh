#!/bin/bash
LogName=$(printf "%s/%s_%s_$1.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
IpVer="$2"

source inc.sh

PhyItf="eth0"
VlanItf="$PhyItf.$VlanId"

fEnvGetRemote

fRemoteFwallUpdate()
{
	fRemoteCmd "ip6tables -A OUTPUT -o $VlanItf -p ipv6-icmp -m icmp6 --icmpv6-type 128 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $VlanItf -p ipv6-icmp -m icmp6 --icmpv6-type 135 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $VlanItf -p ipv6-icmp -m icmp6 --icmpv6-type 136 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -p udp --sport 53 -o bridge0 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $VlanItf -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p udp --dport 53 -j ACCEPT"

	fRemoteFwallLog "iptables" "n"
	fRemoteFwallLog "ip6tables" "n"
}

fIcmp()
{
	fIcmpRemoteExt "$IpVer" "$1"
}

case "$Action" in
	on)
		fRemoteFwallReset

		fRemoteConnectivityPrepare

		fRemoteLan "check" "$PhyItf"

		fRemoteCmdSwiQcmap "vlan create name $VlanItf if $PhyItf id $VlanId ipv6 $VlanArIpv6Addr"
		fRemoteLan "check" "$VlanItf"

		fRemoteWwan "on"
		fRemoteWwan "check"

		fRemoteFwallUpdate
		;;
	off)
		fRemoteWwan "off"

		fRemoteCmdSwiQcmap "vlan del if $PhyItf id $VlanId"

		fRemoteFwallReset
		;;
	play)
		fRemoteLan "check" "$PhyItf"
		fRemoteLan "check" "$VlanItf"
		fRemoteWwan "check"

		case "$IpVer" in
			4)
				fIcmp "8.8.8.8"
				fIcmp "www.google.fr"
				;;
			6)
				fIcmp "$VlanUbuntuIpv6Addr"
				;;
		esac
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
