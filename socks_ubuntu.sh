#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2_$3.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
Opt="$2"
IpVer="$3"

source inc.sh

fEnvGetLocal

PhyItf="enp0s25"
VlanItf="$PhyItf.$VlanId"

fIcmp()
{
	if [[ "$Opt" == "vlan" ]]; then
		fIcmpExt "$IpVer" "$1" "$VlanItf"
	else
		fIcmpExt "$IpVer" "$1" "$PhyItf"
	fi
}
fCurlHttpDl()
{
	local Proxy

	Proxy=$(printf "%s socks5://%s" "--proxy" "$2")

	if [[ "$Opt" == "vlan" ]]; then
		fCurlHttpDlExt "$IpVer" "$1" "$VlanItf" "$Proxy"
	else
		fCurlHttpDlExt "$IpVer" "$1" "$PhyItf" "$Proxy"
	fi
}
fCurlHttpUl()
{
	local Proxy

	Proxy=$(printf "%s socks5://%s" "--proxy" "$2")

	if [[ "$Opt" == "vlan" ]]; then
		fCurlHttpUlExt "$IpVer" "$1" "$VlanItf" "$Proxy"
	else
		fCurlHttpUlExt "$IpVer" "$1" "$PhyItf" "$Proxy"
	fi
}
fCurlFtpDl()
{
	local Proxy

	Proxy=$(printf "%s socks5://%s" "--proxy" "$2")

	if [[ "$Opt" == "vlan" ]]; then
		fCurlFtpDlExt "$IpVer" "$1" "$VlanItf" "$Proxy"
	else
		fCurlFtpDlExt "$IpVer" "$1" "$PhyItf" "$Proxy"
	fi
}

case "$Action" in
	on)
		sudo service network-manager stop
		#sudo service network-manager restart

		if [[ "$Opt" == "vlan" ]]; then
			sudo modprobe 8021q
		fi

		fLocalRpFilter "$PhyItf" "off"

		fLocalIpFwd

		#sudo ip addr flush dev "$PhyItf"

		#sudo ip -4 addr add "$EthUbuntuIpv4Addr" dev "$PhyItf"
		sudo ip li set dev "$PhyItf" up

		fLocalLan "check" "$PhyItf"

		if [[ "$Opt" == "vlan" ]]; then
			sudo vconfig add "$PhyItf" "$VlanId"
			sudo ip -4 addr add "$VlanUbuntuIpv4Addr" dev "$VlanItf"
			sudo ip li set dev "$VlanItf" up
			fLocalLan "check" "$VlanItf"

			sudo ip -4 route add "$VlanUbuntuIpv4Nw" dev "$VlanItf"
		fi

		#sudo route add default gw "$BridgeArIpv4Addr"

		if [[ "$Opt" == "vlan" ]]; then
			sudo ifconfig "$VlanItf" mtu $Mtu
		fi
		;;
	off)
		if [[ "$Opt" == "vlan" ]]; then
			sudo vconfig rem "$VlanItf"
		fi

		fLocalRpFilter "$PhyItf" "on"

		sudo service network-manager start
		;;
	playqctexample)
		case "$IpVer" in
			4)
				sudo curl -v $IpVer --proxy socks5://$BridgeArIpv4Addr "http://10.1.1.1/file_to_download.txt"
				sudo curl -v $IpVer --proxy socks5://$BridgeArIpv4Addr --upload-file "file_to_upload.txt" "http://10.1.1.1/"
				sudo curl -v $IpVer --proxy socks5://USERNAME:PASSWORD@$BridgeArIpv4Addr:$ip_Port_SocksProxy https://www.yahoo.com
				;;
			6)
				fBridgeArIpv6AddrGet
				fIcmp "$BridgeArIpv6Addr"

				sudo curl -v -6 --proxy socks5://USERNAME:PASSWORD@[$fBridgeArIpv6AddrGet]:$ip_Port_SocksProxy --get --insecure https://[2001:4998:58:c02::a9] --output /dev/null
				;;
		esac
		;;
	play)
		fLocalLan "check" "$PhyItf"
		if [[ "$Opt" == "vlan" ]]; then
			fLocalLan "check" "$VlanItf"

			fLocalDnsGet "$VlanItf"
		else
			fLocalDnsGet "$PhyItf"
		fi

		case "$IpVer" in
			4)
				fCurlHttpDl "https://$ChinaUnicomGuangdongIpv4Addr2/img/bd_logo1.png" "qti@$BridgeArIpv4Addr"

				#fCurlHttpDl "http://mirrors.ustc.edu.cn/anaconda/miniconda/index.json" "qti@$BridgeArIpv4Addr:$ip_Port_SocksProxy"
				#fCurlHttpDl "http://downapp.baidu.com/baidunuomi/AndroidPhone/8.2.1.4/1/1009769b/20180320110207/baidunuomi_AndroidPhone_8-2-1-4_1009769b.apk" "qti@$BridgeArIpv4Addr:$ip_Port_SocksProxy"

				fCurlFtpDl "ftp://ftp.sjtu.edu.cn/pub/README.NetInstall" "qti@$BridgeArIpv4Addr:$ip_Port_SocksProxy"

				if [[ "$Opt" == "vlan" ]]; then
					fIcmp "$VlanArIpv4Addr"

					fCurlHttpDl "http://www.baidu.com" "qti@$VlanArIpv4Addr"
					#fCurlHttpDl "$ChinaUnicomGuangdongIpv4Addr1" "qti@$VlanArIpv4Addr"
					#fCurlHttpDl "$ChinaUnicomGuangdongIpv4Addr2" "qti@$VlanArIpv4Addr"
					fCurlHttpDl "http://$ChinaUnicomGuangdongIpv4Addr2/img/bd_logo1.png" "qti@$VlanArIpv4Addr"
				fi
				;;
			6)
				fBridgeArIpv6AddrGet
				fIcmp "$BridgeArIpv6Addr"

				fCurlHttpDl "http://www.baidu.com/img/bd_logo1.png" "qti@[\"$BridgeArIpv6Addr\"]:$ip_Port_SocksProxy"

				#fCurlHttpDl "http://downapp.baidu.com/baidunuomi/AndroidPhone/8.2.1.4/1/1009769b/20180320110207/baidunuomi_AndroidPhone_8-2-1-4_1009769b.apk" "[$BridgeArIpv6Addr]:$ip_Port_SocksProxy"
				#fCurlHttpDl "http://mirrors.ustc.edu.cn/anaconda/miniconda/index.json" "[$BridgeArIpv6Addr]:$ip_Port_SocksProxy"

				fCurlFtpDl "ftp://ftp.sjtu.edu.cn/pub/README.NetInstall" "qti@[\"$BridgeArIpv6Addr\"]:$ip_Port_SocksProxy"
				;;
		esac
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
