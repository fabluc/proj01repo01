#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2_$3.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
Opt="$2"
IpVer="$3"

source inc.sh

fEnvGetLocal

PhyItf="enp0s25"

fIcmp()
{
	fIcmpExt "$IpVer" "$1" "$PhyItf"
}

fIpPerfs()
{
	local Params="$1"
	local Log="$2"
	local Port="$3"
	local Cmd=""
	local CurrDir
	local LogsDir

	CurrDir=$(pwd)
	LogsDir=$(printf "%s/logs" "$CurrDir")

	Cmd="iperf"
	if [ ! -z "$Params" ]; then
		Cmd+=$(printf " %s" "$Params")
	fi
	if [ ! -z "$Log" ]; then
		Cmd+=$(printf " -o %s/%s" "$LogsDir" "$Log")
	fi
	if [ ! -z "$Port" ]; then
		Cmd+=$(printf " -p %s" "$Port")
	fi

	Cmd+=" --print_mss"
	Cmd+=" --enhanced"
	Cmd+=" --interval 2"
	Cmd+=" --ipv6_domain"

	$Cmd

	ls -alt "$LogsDir/$Log"
	if [ -f "$LogsDir/$Log" ]; then
		gedit "$LogsDir/$Log" &
	fi
}

fIpPerfsSrv()
{
	local Proto="$1"
	local Port="$2"
	local Params=""
	local Log

	LogSuffix=".log"
	Log=$(printf "%s" "$Stamp")
	Log+=$(printf "_iperf_srv_%s" "$Proto")
	Log+=$(printf "%s" "$LogSuffix")

	Params+=" -s"
	if [ "$Proto" == "udp" ]; then
		Params+=" --udp"
	fi

	fIpPerfs "$Params" "$Log" "$Port"
}

fIpPerfsCli()
{
	local Proto="$1"
	local Srv="$2"
	local Port="$3"
	local Params=""
	local Log

	LogSuffix=".log"
	Log=$(printf "%s" "$Stamp")
	Log+=$(printf "_iperf_cli_%s" "$Proto")
	Log+=$(printf "%s" "$LogSuffix")

	if [ -z "$Srv" ]; then
		Srv="localhost"
	fi
	Params+=" -c $Srv"

	Params+=" --dualtest"
	#Params+=" --bandwidth=2000m"
	Params+=" --bandwidth 1G"

	if [ "$Proto" == "udp" ]; then
		Params+=" --udp"
	fi

	fIpPerfs "$Params" "$Log" "$Port"
}

fIpPerfs3()
{
	local Params="$1"
	local Log="$2"
	local Port="$3"
	local Cmd=""
	local CurrDir
	local LogsDir

	CurrDir=$(pwd)
	LogsDir=$(printf "%s/logs" "$CurrDir")

	Cmd="iperf3"
	if [ ! -z "$Params" ]; then
		Cmd+=$(printf " %s" "$Params")
	fi
	if [ ! -z "$Log" ]; then
		Cmd+=$(printf " --logfile %s/%s" "$LogsDir" "$Log")
	fi
	if [ ! -z "$Port" ]; then
		Cmd+=$(printf " -p %s" "$Port")
	fi
	Cmd+=" --verbose"
	#Cmd+=" --debug"
	$Cmd

	ls -alt "$LogsDir/$Log"
	if [ -f "$LogsDir/$Log" ]; then
		less -R "$LogsDir/$Log"
	fi

	fKeyToContinue
}

fIpPerfs3Srv()
{
	local Proto="$1"
	local Port="$2"
	local IpVer="$3"
	local Params=""
	local Log

	LogSuffix=".log"
	Log=$(printf "%s" "$Stamp")
	Log+=$(printf "_iperf_srv_%s" "$Proto")
	Log+=$(printf "%s" "$LogSuffix")

	Params+=" --server"
	Params+=" -$IpVer"

	# Disabled to allow preliminary nc
	#Params+=" --one-off"

	fIpPerfs3 "$Params" "$Log" "$Port"

	netstat --statistics --$Proto; fRetCheck
}

fIpPerfs3Cli()
{
	local Proto="$1"
	local Srv="$2"
	local Port="$3"
	local IpVer="$4"
	local Params=""
	local Log

	sysctl net.core.rmem_default
	sysctl net.ipv4.tcp_rmem
	sysctl net.ipv4.udp_mem
	if false; then
		# If UDP test fails due to too low kernel default socket buffer size:
		sysctl -w net.core.rmem_default=655360
		sysctl -w net.ipv4.tcp_rmem="655360 6553600 131072000"
		sysctl -w net.ipv4.udp_mem="655360 6553600 131072000"
	fi

	nc -vv -z "$Srv" "$Port"; fRetCheck
	nmap -v -p "$Port" "$Srv"; fRetCheck

	LogSuffix="iperf.log"
	Log=$(printf "%s" "$Stamp")
	Log+=$(printf "_cli_%s_%s_%s:%s" "$IpVer" "$Proto" "$Srv" "$Port")
	Log+=$(printf "%s" ".$LogSuffix")

	if [ -z "$Srv" ]; then
		Srv="localhost"
	fi
	Params+=" --client $Srv"

	#Params+=" --reverse"

	#Params+=" --bandwidth 2000m"
	Params+=" --bandwidth 1G"
	Params+=" --time 10"
	Params+=" -$IpVer"

	if [ "$Proto" == "udp" ]; then
		Params+=" --udp"
	fi

	fIpPerfs3 "$Params" "$Log" "$Port"

	netstat --statistics --$Proto; fRetCheck
}

sudo service network-manager stop

CurrDir=$(pwd)
LogsDir=$(printf "%s/logs" "$CurrDir")

case "$Action" in
	iperf2)
		case "$Opt" in
			srv)
				fIpPerfsSrv "$IpPerfsProto"
				;;
			cli)
				case "$IpVer" in
					4)
						Srv="bouygues.iperf.fr"; Port="9222"
						#Srv="ping.online.net"; Port="5209"
						#Srv="ping-90ms.online.net"; Port="5209"
						#Srv="speedtest.serverius.net"; Port="5002"
						#Srv="iperf.eenet.ee"; Port="5201"
						#Srv="iperf.volia.net"; Port="5201"
						#Srv="iperf.it-north.net"; Port="5209"
						#Srv="iperf.biznetnetworks.com"; Port="5203"
						#Srv="iperf.scottlinux.com"; Port="5201"
						#Srv="iperf.he.net"; Port="5201"

						fIcmp "$Srv"
						fIpPerfsCli "$IpPerfsProto" "$Srv" "$Port"
						;;
					6)
						fBridgeArIpv6AddrGet

						#Srv="localhost"; Port="$ip_Port_Iperf"
						#Srv="ping6.online.net"; Port="5209"
						#Srv="ping6-90ms.online.net"; Port="5209"
						Srv="$BridgeArIpv6Addr"; Port="$ip_Port_Iperf"

						fIcmp "$Srv"
						fIpPerfsCli "$IpPerfsProto" "$Srv" "$Port"
						;;
				esac
				;;
		esac
		;;
	iperf3)
		case "$Opt" in
			srv)
				fIpPerfs3Srv "$IpPerfsProto" "$ip_Port_Iperf" "$IpVer"
				;;
			cli)
				case "$IpVer" in
					4)
						#Srv="localhost"; Port="$ip_Port_Iperf"
						#Srv="bouygues.iperf.fr"; Port="9222"
						#Srv="ping.online.net"; Port="5209"
						#Srv="ping-90ms.online.net"; Port="5209"
						#Srv="localhost"; Port="$ip_Port_Iperf"
						Srv="$BridgeArIpv4Addr"; Port="$ip_Port_Iperf"

						if false; then
							# iperf3: error - unable to connect to server: Connection timed out
							#Srv="iperf.eenet.ee"; Port="5201"
							#Srv="speedtest.serverius.net"; Port="5002"
							Srv="iperf.volia.net"; Port="5201"
							#Srv="iperf.it-north.net"; Port="5209"
							#Srv="iperf.biznetnetworks.com"; Port="5203"
							#Srv="iperf.scottlinux.com"; Port="5201"
							#Srv="iperf.he.net"; Port="5201"
						fi

						if [ "$Srv" != "localhost" ]; then
							fIcmp "$Srv"
						fi
						;;
					6)
						fBridgeArIpv6AddrGet

						#Srv="localhost"; Port="$ip_Port_Iperf"
						#Srv="bouygues.iperf.fr"; Port="9222"
						#Srv="ping6.online.net"; Port="5209"
						#Srv="ping6-90ms.online.net"; Port="5209"
						#Srv="speedtest.serverius.net"; Port="5002"
						#Srv="iperf.biznetnetworks.com"; Port="5203"
						#Srv="iperf.scottlinux.com"; Port="5201"
						#Srv="iperf.he.net"; Port="5201"
						Srv="$BridgeArIpv6Addr"; Port="$ip_Port_Iperf"

						if [ "$Srv" != "localhost" ]; then
							fIcmp "$Srv"
						fi
						;;
				esac

				fIpPerfs3Cli "$IpPerfsProto" "$Srv" "$Port" "$IpVer"
				;;
		esac
		;;
	*)
		;;
esac

cd "$CurrDir"

} 2>&1 | tee "$LogName"
