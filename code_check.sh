#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2_$3.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

source inc.sh

Files=$(find "." -name "*.sh" -type f)
for File in $Files; do
	shellcheck "$File" --exclude=SC2059,SC2016,SC2027,SC2140,SC2034,SC2029,SC2129,SC2086,SC2039 -x
	fKeyToContinue
done

} 2>&1 | tee "$LogName"
