#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
Opt="qcmap"
IpVer="$2"

source inc.sh

fEnvGetRemote

PhyItf="eth0"
VlanItf="$PhyItf.$VlanId"

fRemoteFwallUpdate()
{
	fRemoteCmd "ip6tables -A FORWARD -i bridge0 -o $WwanQcmapItf -m physdev --physdev-in $L2tpItf -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i bridge0 -p tcp -o $WwanQcmapItf -m physdev --physdev-in $L2tpItf -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i bridge0 -p udp -o $WwanQcmapItf -m physdev --physdev-in $L2tpItf -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i eth0 -p tcp --dport $ip_Port_Http -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i eth0 -p tcp --sport $ip_Port_Http -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i $VlanItf -o $VlanItf -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i $WwanQcmapItf -o bridge0 -p tcp -m tcp --sport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i $WwanQcmapItf -o bridge0 -p tcp --sport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i $WwanQcmapItf -o bridge0 -p udp -m udp --sport $ip_Port_Ntp -j DROP"
	fRemoteCmd "ip6tables -A FORWARD -i $WwanQcmapItf -o $WwanQcmapItf -p tcp -m tcp --sport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -i $WwanQcmapItf -o $WwanQcmapItf -p udp -m udp --sport $ip_Port_Ntp -j DROP"
	fRemoteCmd "ip6tables -A FORWARD -p icmpv6 --icmpv6-type echo-reply -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p icmpv6 --icmpv6-type echo-request -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p ipv6-icmp -m icmp6 --icmpv6-type 1 -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p ipv6-icmp -m icmp6 --icmpv6-type 3 -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p ipv6-icmp -m icmp6 --icmpv6-type echo-reply -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p ipv6-icmp -m icmp6 --icmpv6-type echo-request -j ACCEPT"
	fRemoteCmd "ip6tables -A INPUT -i $WwanQcmapItf -p tcp -m tcp --sport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 135 -j ACCEPT"
	fRemoteCmd "ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 136 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $VlanItf -p $ip_Port_L2tp -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_Http -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p udp --sport 546 --dport 547 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 129 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 131 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 132 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 133 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 134 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 135 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 136 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 137 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 143 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p ipv6-icmp -m icmp6 --icmpv6-type 1 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i bridge0 -p icmp -m icmp --icmp-type 3 -o $WwanQcmapItf -m physdev --physdev-in $L2tpItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i bridge0 -p tcp -o $WwanQcmapItf -m physdev --physdev-in $L2tpItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i bridge0 -p udp -o $WwanQcmapItf -m physdev --physdev-in $L2tpItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -o $WwanQcmapItf -p icmp -m icmp --icmp-type 3 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -o $WwanQcmapItf -p tcp --sport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -p tcp --dport 53 -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -p udp --dport 53 -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -p udp --dport $ip_Port_Ntp -o $WwanQcmapItf -j DROP"
	fRemoteCmd "iptables -A FORWARD --in-interface $L2tpItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -o bridge0 -p tcp -m tcp --sport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -o bridge0 -p tcp --sport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -p udp --sport 53 -o bridge0 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -p udp --sport $ip_Port_Ntp -o bridge0 -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp --dport 1124 -m physdev --physdev-in $L2tpItf -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp --dport 137 -m physdev --physdev-in $L2tpItf -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp --dport 138 -m physdev --physdev-in $L2tpItf -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp --dport 3289 -j DROP"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp -m udp --sport 67 --dport 68 -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p udp --sport 8612 -m physdev --physdev-in $L2tpItf -j DROP"
	fRemoteCmd "iptables -A INPUT -i $WwanQcmapItf -p icmp -m icmp --icmp-type 0 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p icmp -m icmp --icmp-type 0 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p icmp -m icmp --icmp-type 11 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p icmp -m icmp --icmp-type 3 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o lo -p icmp -m icmp --icmp-type 3 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o lo -p udp --dport 53 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $VlanItf -p icmp -m icmp --icmp-type 136 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $VlanItf -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpData -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_Http -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_HttpSec -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p udp --dport 53 -j ACCEPT"

	fRemoteFwallLog "iptables" "n"
	fRemoteFwallLog "ip6tables" "n"
}

fIcmp()
{
	case "$Opt" in
		qcmap)
			fIcmpRemoteExt "$IpVer" "$1" "$WwanQcmapItf"
			;;
		mdc)
			fIcmpRemoteExt "$IpVer" "$1" "$WwanMdcItf"
			;;
	esac
}
fCurlHttpDl()
{
	case "$Opt" in
		qcmap)
			fCurlRemoteHttpDlExt "$IpVer" "$1" "$WwanQcmapItf"
			;;
		mdc)
			fCurlRemoteHttpDlExt "$IpVer" "$1" "$WwanMdcItf"
			;;
	esac
}
fCurlFtpDl()
{
	case "$Opt" in
		qcmap)
			fCurlRemoteFtpDlExt "$IpVer" "$1" "$WwanQcmapItf"
			;;
		mdc)
			fCurlRemoteFtpDlExt "$IpVer" "$1" "$WwanMdcItf"
			;;
	esac
}

case "$Action" in
	on)
		fRemoteFwallReset

		fRemoteConnectivityPrepare

		fRemoteLan "check" "$PhyItf"

		fRemoteCmdSwiQcmap "vlan create name $VlanItf if $PhyItf id $VlanId ipv6 $VlanArIpv6Addr"
		fRemoteLan "check" "$VlanItf"

		fRemoteCmdSwiQcmap "l2tp set state 1 tcpmssavoidfrag 1 mtuavoidfrag 1"
		fRemoteCmdSwiQcmap "l2tp create vname $VlanItf ltid 1 rtid 1 lsid 1 rsid 1 ipfamily 2 ipv6 $L2tpUbuntuIpv6Addr mtu $Mtu"
		fRemotePhyItfWait "$L2tpItf"

		fRemoteCmd "cat /sys/kernel/debug/l2tp/tunnels"

		fRemoteWwan "on"
		fRemoteWwan "check"

		fRemoteFwallUpdate
		;;
	off)
		fRemoteWwan "off"

		fRemoteCmdSwiQcmap "l2tp del ltid 1"
		fRemoteCmdSwiQcmap "l2tp set state 0 tcpmssavoidfrag 1 mtuavoidfrag 1"

		fRemoteCmdSwiQcmap "vlan del if $PhyItf id $VlanId"

		fRemoteFwallReset
		;;
	play)
		fRemoteLan "check" "$PhyItf"
		fRemoteLan "check" "$VlanItf"
		fRemoteWwan "check"

		case "$IpVer" in
			4)
				fIcmpRemoteExt "$IpVer" "$L2tpUbuntuIpv4Addr"

				fIcmp "8.8.8.8"
				fIcmp "www.google.fr"
				fCurlHttpDl "http://www.google.fr"

				fIcmp "www.yahoo.fr"
				fCurlHttpDl "http://www.yahoo.fr"

				fIcmp "ftp.sjtu.edu.cn"
				fCurlFtpDl "ftp://ftp.sjtu.edu.cn/pub/README.NetInstall"
				;;
			6)
				fIcmpRemoteExt "$IpVer" "$L2tpUbuntuIpv6Addr"

				fIcmp "2001:4860:4860::8888"
				fIcmp "www.google.fr"
				fCurlHttpDl "http://www.google.fr"

				fIcmp "ftp6.sjtu.edu.cn"
				fCurlFtpDl "ftp://ftp6.sjtu.edu.cn/pub/README.NetInstall"

				if false; then
					# 2 packets transmitted, 0 packets received, 100% packet loss
					fIcmp "$ChinaTelecomGuangdongIpv6Addr"

					# 2 packets transmitted, 0 packets received, 100% packet loss
					fIcmp "ipv6.baidu.com"

					fCurlHttpDl "http://ipv6.baidu.com/img/bd_logo1.png"
				fi
				;;
		esac
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
