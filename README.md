git clone https://fabluc@bitbucket.org/fabluc/proj01repo01.git

# Adapt Settings/Variables to your Environment

## Inside inc.sh

```
ArBuildOfficial
DmCaptDir
VlanId
ArUsbDmDev
```

## Inside several *.sh files

```
PhyItf
```

# Configure AR UARTs

```
./uart_ar.sh cfg
```

# Execution Logs

1 log per *.sh

Path:
```
./logs/
```

# Tests

## Ubuntu Firewall

```
./firewall_ubuntu.sh get

./firewall_ubuntu.sh log
```

## AR Firewall

```
./firewall_ar.sh save|restore

./firewall_ar.sh get

./firewall_ar.sh reset

./firewall_ar.sh mon
	Ctrl+C to stop
```

## Ubuntu LAN

```
./lan_ubuntu.sh dhcp
```

## AR LAN

```
./lan_ar.sh on

...

./lan_ar.sh off
```

```
./lan_ar.sh onoffloop
```

## Ubuntu WLAN (purpose: setup tests without involving AR)

```
If no IP address assigned to WLAN interface:
	./wlan_ubuntu.sh dhcp

./wlan_ubuntu.sh on

./wlan_ubuntu.sh play "" 4
only if IPv6 available on your WLAN:
	./wlan_ubuntu.sh play "" 6

./wlan_ubuntu.sh stress dl|ul 4
only if IPv6 available on your WLAN:
	./wlan_ubuntu.sh stress dl|ul 6
...

./wlan_ubuntu.sh off
```

## Ubuntu LAN + AR LAN+WWAN

```
./debug.sh dmusb2loc on
or
./debug.sh dmip2loc on
or
./debug.sh dmusb2remote on

./firewall_ar.sh mon
	Ctrl+C to stop

./perf_ar.sh cpu logon

./debug.sh iploc on

./debug.sh iprem on

./debug.sh ipa on

./debug.sh ipa mon
	Ctrl+C to stop

./wwan_ubuntu.sh on

./wwan_ar.sh on qcmap
or
./wwan_ar.sh on mdc

./wwan_ar.sh status qcmap
or
./wwan_ar.sh status mdc

./wwan_ar.sh play qcmap 4
or
./wwan_ar.sh play mdc 4
if IPv6 available on your WWAN:
	./wwan_ar.sh play qcmap 6
	or
	./wwan_ar.sh play mdc 6

./wwan_ubuntu.sh play "" 4
if IPv6 available on your WWAN:
	./wwan_ubuntu.sh play "" 6

./wwan_ubuntu.sh stress dl|ul 4
if IPv6 available on your WWAN:
	./wwan_ubuntu.sh stress dl|ul 6

...

./wwan_ar.sh off qcmap
or
./wwan_ar.sh off mdc

./wwan_ubuntu.sh off

./debug.sh ipa off

./perf_ar.sh cpu logoff

./debug.sh iprem off
./debug.sh iploc off

./debug.sh dmusb2loc off
or
./debug.sh dmip2loc off
or
./debug.sh dmusb2remote off
```

## Ubuntu LAN+VLAN + AR LAN+VLAN+WWAN

```
./debug.sh dmusb2loc on
or
./debug.sh dmip2loc on
or
./debug.sh dmusb2remote on

./firewall_ar.sh mon
	Ctrl+C to stop

./debug.sh iploc on
./debug.sh iprem on

./perf_ar.sh cpu logon

./debug.sh ipa on

./debug.sh ipa mon
	Ctrl+C to stop

./vlan_ubuntu.sh on

./vlan_ar.sh on

./vlan_ar.sh play 4
if IPv6 available on your WWAN:
	./vlan_ar.sh play 6

./vlan_ubuntu.sh play 4
if IPv6 available on your WWAN:
	./vlan_ubuntu.sh play 6

...

./vlan_ar.sh off

./vlan_ubuntu.sh off

./debug.sh ipa off

./perf_ar.sh cpu logoff

./debug.sh iprem off
./debug.sh iploc off

./debug.sh dmusb2loc off
or
./debug.sh dmip2loc off
or
./debug.sh dmusb2remote off
```

## Ubuntu LAN+VLAN+L2TPv3 + AR LAN+VLAN+L2TPv3+WWAN

```
./debug.sh dmusb2loc on
or
./debug.sh dmip2loc on
or
./debug.sh dmusb2remote on

./firewall_ar.sh mon
	Ctrl+C to stop

./perf_ar.sh cpu logon

./debug.sh iploc on
./debug.sh iprem on

./debug.sh ipa on

./debug.sh ipa mon
	Ctrl+C to stop

./l2tp_ubuntu.sh on

./l2tp_ar.sh on

./l2tp_ar.sh play 4
if IPv6 available on your WWAN:
	./l2tp_ar.sh play 6

./l2tp_ubuntu.sh play "" 4
if IPv6 available on your WWAN:
	./l2tp_ubuntu.sh play "" 6

./l2tp_ubuntu.sh stress dl|ul 4
if IPv6 available on your WWAN:
	./l2tp_ubuntu.sh stress dl|ul 6

./l2tp_ubuntu.sh scan

...

./l2tp_ar.sh off

./l2tp_ubuntu.sh off

./debug.sh ipa off

./perf_ar.sh cpu logoff

./debug.sh iprem off
./debug.sh iploc off

./debug.sh dmusb2loc off
or
./debug.sh dmip2loc off
or
./debug.sh dmusb2remote off
```

## Ubuntu LAN(+VLAN)+SOCKSv5 + AR LAN(+VLAN)+SOCKSv5+WWAN

```
./debug.sh dmusb2loc on
or
./debug.sh dmip2loc on
or
./debug.sh dmusb2remote on

./firewall_ar.sh mon
	Ctrl+C to stop

./debug.sh iploc on
./debug.sh iprem on

./perf_ar.sh cpu logon

./debug.sh ipa on

./debug.sh ipa mon
	Ctrl+C to stop

./socks_ubuntu.sh on [vlan]

./socks_ar.sh onpart1 [vlan]
./socks_ar.sh onpart2 [vlan]

./socks_ubuntu.sh play [vlan] 4
if IPv6 available on your WWAN:
	./socks_ubuntu.sh play [vlan] 6

./socks_ar.sh play [vlan] 4
if IPv6 available on your WWAN:
	./socks_ar.sh play [vlan] 6

...

./socks_ar.sh off [vlan]

./socks_ubuntu.sh off [vlan]

./debug.sh ipa off

./perf_ar.sh cpu logoff

./debug.sh iprem off
./debug.sh iploc off

./debug.sh dmusb2loc off
or
./debug.sh dmip2loc off
or
./debug.sh dmusb2remote off
```

## Performances: Ubuntu LAN + AR LAN(+WWAN)

```
./firewall_ar.sh mon
	Ctrl+C to stop

./perf_ubuntu.sh iperf2|iperf3 srv|cli 4|6
	Ctrl+C to stop

If iperf server located on WWAN:
	./wwan_ar.sh on qcmap
	or
	./wwan_ar.sh on mdc

./perf_ar.sh iperf3 srv|cli 4|6 on

...

./perf_ar.sh cpu stress|mon|monfull|logon|logoff
./perf_ar.sh cpu anal <log>

./perf_ar.sh iperf3 srv|cli 4|6 off
If iperf server located on WWAN:
	./wwan_ar.sh off qcmap
	or
	./wwan_ar.sh off mdc
```

# Want to check scripts implementation?

```
./code_check.sh
```
