#!/bin/bash
LogName=$(printf "%s/%s_%s_$1.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"

source inc.sh

fEnvGetLocal

PhyItf="enp0s25"

case "$Action" in
	dhcp)
		fDhcpRenew "$PhyItf"
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
