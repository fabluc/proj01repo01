#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
Opt="$2"
IpVer="$3"

source inc.sh

fEnvGetRemote

PhyItf="eth0"
VlanItf="$PhyItf.$VlanId"

fRemoteFwallUpdate()
{
	fRemoteCmd "ip6tables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p ipv6-icmp -m icmp6 --icmpv6-type echo-reply -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p ipv6-icmp -m icmp6 --icmpv6-type echo-request -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p tcp --dport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "ip6tables -A FORWARD -p tcp --sport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "ip6tables -A INPUT -p tcp --dport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "ip6tables -A INPUT -p tcp --sport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o bridge0 -p tcp -m tcp --sport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o bridge0 -p udp -m udp --sport 53 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $VlanItf -p tcp --dport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $VlanItf -p tcp --sport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_Http -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_HttpSec -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p udp -m udp --dport 53 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o $WwanQcmapItf -p udp -m udp --sport 53 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p tcp --dport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -p tcp --sport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "ip6tables -t nat -A POSTROUTING -o $WwanQcmapItf -j MASQUERADE"
	fRemoteCmd "iptables -A FORWARD -i eth0 -p tcp --dport 53 -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i eth0 -p udp --dport 53 -o $WwanQcmapItf -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -p udp --dport 53 -o bridge0 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -i $WwanQcmapItf -p udp --sport 53 -o bridge0 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -o $WwanQcmapItf -p udp --dport 53 -i bridge0 -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -p tcp --dport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "iptables -A FORWARD -p tcp --sport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -p tcp --dport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -p tcp --sport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p tcp -m tcp --dport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p tcp -m tcp --sport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p udp -m udp --sport 53 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $VlanItf -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $VlanItf -p tcp --dport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $VlanItf -p tcp --sport $ip_Port_SocksProxy -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p icmp -m icmp --icmp-type 8 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_FtpCtrl -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_Http -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p tcp -m tcp --dport $ip_Port_HttpSec -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p udp --dport 53 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o $WwanQcmapItf -p udp -m udp --sport 53 -j ACCEPT"
	fRemoteCmd "iptables -t nat -A POSTROUTING -o $WwanQcmapItf -j MASQUERADE"

	fRemoteFwallLog "iptables" "n"
	fRemoteFwallLog "ip6tables" "n"
}

fSocksCfgGet()
{
	echo '
	QCMAP_CLI
		3. NAT/ALG/VPN Configuration
		. Get SOCKSv5 Proxy Config
	'
	#fRemoteCmd "QCMAP_CLI"

	fRemoteCmd "cat /etc/qct_cfg/qti_socksv5_auth.xml"
	fKeyToContinue
	fRemoteCmd "cat /etc/qct_cfg/qti_socksv5_conf.xml"
	fKeyToContinue
}

fIcmp()
{
	case "$Opt" in
		qcmap)
			fIcmpRemoteExt "$IpVer" "$1" "$2" "$WwanQcmapItf"
			;;
		mdc)
			fIcmpRemoteExt "$IpVer" "$1" "$2" "$WwanMdcItf"
			;;
	esac
}

case "$Action" in
	onpart1)
		fRemoteFwallReset

		fRemoteConnectivityPrepare

		fRemoteLan "check" "$PhyItf"

		if [[ "$Opt" == "vlan" ]]; then
			fRemoteCmdSwiQcmap "vlan create name $VlanItf if $PhyItf id $VlanId ip $VlanArIpv4Addr"
			fRemoteLan "check" "$VlanItf"
		fi

		echo '
		QCMAP_CLI

		1. MobileAP Configuration
		2. Enable/Disable mobileap
		Please input MobileAP State(1-Enable/0-Disable) : 1

		OPTIONAL IF CONFIG ALREADY DONE
			3. NAT/ALG/VPN Configuration
			23. Set SOCKSv5 Proxy Config
			4. Add/Delete Username/Profile Association
			(0-Add/1-Delete) : 0
			Please Enter Username: swi
			Please Enter Service/Profile#: 1

		OPTIONAL IF CONFIG ALREADY DONE
			3. NAT/ALG/VPN Configuration
			23. Set SOCKSv5 Proxy Config
			3. Edit LAN Interface
			Please Enter new SOCKSv5 Proxy LAN iface: bridge0 or $VlanItf

		OPTIONAL IF CONFIG ALREADY DONE
			3. NAT/ALG/VPN Configuration
			23. Set SOCKSv5 Proxy Config
			2. Set SOCKSv5 Proxy Authentication Method
			Please Enter SOCKSv5 Proxy Authentication Method (0- No Authentication, 1 - Username/Password): 1

		3. NAT/ALG/VPN Configuration
		22. Enable/Disable SOCKSv5Proxy
		Please input SOCKSv5 Proxy State(1-Enable/0-Disable) : 1
		'
		fRemoteCmd "QCMAP_CLI"
		;;
	onpart2)
		fSocksCfgGet

		fRemoteWwan "on"
		fRemoteWwan "check"

		#fRemoteCmd "ip route add $ChinaUnicomGuangdongIpv4Addr1 dev rmnet_data0"
		fRemoteCmd "ip route add $ChinaUnicomGuangdongIpv4Addr1 dev rmnet_data1"
		fRemoteCmd "ip route add $ChinaUnicomGuangdongIpv4Addr1 dev rmnet_data2"
		#fRemoteCmd "ip route add $ChinaUnicomGuangdongIpv4Addr2 dev rmnet_data0"
		fRemoteCmd "ip route add $ChinaUnicomGuangdongIpv4Addr2 dev rmnet_data1"
		fRemoteCmd "ip route add $ChinaUnicomGuangdongIpv4Addr2 dev rmnet_data2"

		fRemoteFwallUpdate
		;;
	off)
		fRemoteWwan "off"

		if [[ "$Opt" == "vlan" ]]; then
			fRemoteCmdSwiQcmap "vlan del if $PhyItf id $VlanId"
		fi

		fRemoteFwallReset
		;;
	play)
		fRemoteLan "check" "$PhyItf"
		if [[ "$Opt" == "vlan" ]]; then
			fRemoteLan "check" "$VlanItf"
		fi

		fRemoteWwan "check"

		case "$IpVer" in
			4)
				#fIcmpRemoteExt "$IpVer" "$EthUbuntuIpv4Addr"
				#fIcmpRemoteExt "$IpVer" "$EthUbuntuIpv4Addr" "bridge0"

				if [[ "$Opt" == "vlan" ]]; then
					fIcmpRemoteExt "$IpVer" "$VlanUbuntuIpv4Addr"
					fIcmpRemoteExt "$IpVer" "$VlanUbuntuIpv4Addr" "$VlanItf"
					fIcmpRemoteExt "$IpVer" "$VlanUbuntuIpv4Addr" "bridge0"
				fi

				fIcmp "8.8.8.8"
				;;
			6)
				;;
		esac

		fIcmp "www.google.fr"

		fIcmp "www.baidu.com"
		fIcmp "$ChinaUnicomGuangdongIpv4Addr1"
		fIcmp "$ChinaUnicomGuangdongIpv4Addr2"
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
