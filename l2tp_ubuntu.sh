#!/bin/bash
LogName=$(printf "%s/%s_%s_$1_$2_$3.log" "logs" "$(date +%Y%m%d-%H%M%S-%Z)" "$(basename $0)")
{

Action="$1"
Opt="$2"
IpVer="$3"

source inc.sh

fEnvGetLocal

PhyItf="enp0s25"
VlanItf="$PhyItf.$VlanId"

fIcmp()
{
	fIcmpExt "$IpVer" "$1" "$L2tpItf"
}
fCurlHttpDl()
{
	fCurlHttpDlExt "$IpVer" "$1" "$L2tpItf"
}
fCurlHttpUl()
{
	fCurlHttpUlExt "$IpVer" "$1" "$L2tpItf"
}
fCurlFtpDl()
{
	fCurlFtpDlExt "$IpVer" "$1" "$L2tpItf"
}

case "$Action" in
	on)
		sudo service network-manager stop

		sudo modprobe 8021q
		sudo modprobe l2tp_debugfs
		sudo modprobe l2tp_eth
		#sudo modprobe l2tp_ip
		#sudo modprobe l2tp_ip6
		sudo modprobe l2tp_core
		sudo modprobe l2tp_netlink

		fLocalRpFilter "$PhyItf" "off"

		fLocalIpFwd

		sudo ip addr flush dev "$PhyItf"
		sudo ip li set dev "$PhyItf" up

		fLocalLan "check" "$PhyItf"

		sudo vconfig add "$PhyItf" "$VlanId"
		sudo ip -6 addr add "$VlanUbuntuIpv6Addr" dev "$VlanItf"
		sudo ip li set dev "$VlanItf" up
		fLocalLan "check" "$VlanItf"

		sudo ip -6 route add "$VlanUbuntuIpv6Nw" dev "$VlanItf"

		while true; do
			sudo ip -6 l2tp add remote $L2tpArIpv6Addr local $L2tpUbuntuIpv6Addr tunnel tunnel_id 1 peer_tunnel_id 1 encap ip
			if [[ $? -ne 0 ]]; then
				printf "${DisplWrn}%s NOK${DisplNormal}\n" "$0:$LINENO"
				sleep 1
			else
				printf "${DisplOk}%s OK${DisplNormal}\n" "$0:$LINENO"
				break
			fi
		done
		sudo ip -6 l2tp add session tunnel_id 1 session_id 1 peer_session_id 1
		if true; then
			sudo ifconfig $L2tpItf $L2tpUbuntuIpv4Addr netmask $L2tpUbuntuIpv4Nw up
		else
			sudo ip addr add $L2tpUbuntuIpv4Addr dev $L2tpItf
			sudo ip route add $L2tpUbuntuIpv4Nw dev $L2tpItf
		fi
		#sudo ip li set dev $L2tpItf up
		sudo ifconfig $L2tpItf up

		fLocalLan "$L2tpItf"

		sudo route add default gw "$BridgeArIpv4Addr"

		sudo ifconfig "$PhyItf" mtu $Mtu
		sudo ifconfig "$VlanItf" mtu $Mtu

		sudo cat /sys/kernel/debug/l2tp/tunnels
		;;
	off)
		sudo ip -6 l2tp del session tunnel_id 1 session_id 1
		sudo ip -6 l2tp del tunnel tunnel_id 1

		sudo vconfig rem "$VlanItf"

		fLocalRpFilter "$PhyItf" "on"

		#sudo service network-manager start
		;;
	play)
		fLocalLan "check" "$PhyItf"

		fLocalDnsGet "$L2tpItf"

		case "$IpVer" in
			4)
				fIcmp "8.8.8.8"
				fIcmp "www.google.fr"
				fCurlHttpDl "http://www.google.fr"

				fIcmp "www.yahoo.fr"
				fCurlHttpDl "http://www.yahoo.fr"

				fIcmp "ftp.sjtu.edu.cn"
				fCurlFtpDl "ftp://ftp.sjtu.edu.cn/pub/README.NetInstall"
				;;
			6)
				fIcmp "$ChinaTelecomGuangdongIpv6Addr"
				fCurlHttpDl "http://[$ChinaUnicomGuangdongIpv6Addr]/img/bd_logo1.png"

				fIcmp "ipv6.baidu.com"
				fCurlHttpDl "http://ipv6.baidu.com/img/bd_logo1.png"

				fIcmp "2001:4860:4860::8888"
				fIcmp "www.google.fr"
				fCurlHttpDl "http://www.google.fr"

				fIcmp "2001:a18:1:20::42"
				fIcmp "ipv6forum.com"
				fCurlHttpDl "http://ipv6forum.com"

				fIcmp "ftp2.no.netbsd.org"
				fCurlFtpDl "ftp://ftp2.no.netbsd.org/robots.txt"

				fIcmp "ftp6.sjtu.edu.cn"
				fCurlFtpDl "ftp://ftp6.sjtu.edu.cn/pub/README.NetInstall"
				;;
		esac
		;;
	stress)
		fLocalLan "check" "$PhyItf"

		fLocalDnsGet "$L2tpItf"

		case "$Opt" in
			dl)
				case "$IpVer" in
					4)
						fCurlHttpDl "http://ipv4.bouygues.testdebit.info/10G.iso"

						#fCurlHttpDl "https://ipv4.bouygues.testdebit.info:8080/10G.iso"

						fIcmp "ftp.sjtu.edu.cn"
						fCurlFtpDl "ftp://ftp.sjtu.edu.cn/ubuntu-cd/19.10/ubuntu-19.10-desktop-amd64.iso"
						;;
					6)
						fCurlHttpDl "http://ipv6.bouygues.testdebit.info/10G.iso"

						fCurlHttpDl "https://ipv6.bouygues.testdebit.info:8080/10G.iso"

						#ftp://ftp2.no.netbsd.org/ubuntu-iso/19.10/ubuntu-19.10-desktop-amd64.iso

						fIcmp "ftp6.sjtu.edu.cn"
						fCurlFtpDl "ftp://ftp6.sjtu.edu.cn/ubuntu-cd/19.10/ubuntu-19.10-desktop-amd64.iso"
						;;
				esac
				;;
			ul)
				case "$IpVer" in
					4)
						fCurlHttpUl "https://ptsv2.com/t/ak0n5-1586017009"

						fCurlHttpUl "http://bouygues.testdebit.info"
						fCurlHttpUl "https://bouygues.testdebit.info"
						;;
					6)
						fCurlHttpUl "http://bouygues.testdebit.info"
						fCurlHttpUl "https://bouygues.testdebit.info"
						;;
				esac
				;;
		esac
		;;
	scan)
		nmap -v -Pn -A "$BridgeArIpv4Addr"
		nmap -v -Pn -A "$DnsSrvArIpv4Addr"
		;;
	*)
		;;
esac

} 2>&1 | tee "$LogName"
