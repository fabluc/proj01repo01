#!/bin/bash

set -x
#set +x

Stamp=$(date +%Y%m%d-%H%M%S-%Z)
StampToday=$(date +%Y%m%d)

DisplOk=$(tput setaf 2; tput bold)
DisplErr=$(tput setaf 1; tput bold)
DisplWrn=$(tput setaf 3; tput bold)
DisplBold=$(tput bold)
DisplNormal=$(tput sgr0)

fRetCheck()
{
	local Ret="$?"

	case $Ret in
		0|28)
			printf "${DisplOk}OK${DisplNormal}\n"
			;;
		*)
			printf "${DisplErr}NOK %s${DisplNormal}\n" "$Ret"
			fKeyToContinue
			;;
	esac
}

AutoConnect="1"

Mtu="1938"

DnsSrvArIpv4Addr="192.168.10.110"
DnsSrvUbunuIpv4Addr="127.0.0.53"

VlanId="4"

WwanQcmapProfileId="1"
WwanQcmapItf="rmnet_data0"
WwanMdcProfileId="3"
WwanMdcItf="rmnet_data1"

L2tpItf="l2tpeth0"

EthUbuntuIpv4Addr="192.168.225.21"
EthUbuntuIpv6Addr="fe80::d319:f140:2e65:fc2b"

VlanArIpv4Addr="192.168.245.1"
VlanArIpv6Addr="fd53:7cb8:383:4::123"
#VlanArIpv6Addr="fd53:7cb8:383:4::67"
VlanUbuntuIpv4Addr="192.168.245.2"
VlanUbuntuIpv4Addr2="169.254.14.2"
VlanUbuntuIpv4Nw="192.168.245.0/24"
VlanUbuntuIpv4Nw2="169.254.14.0/24"
VlanUbuntuIpv6Addr="fd53:7cb8:383:4::124"
#VlanUbuntuIpv6Addr="fd53:7cb8:383:4::108"
VlanUbuntuIpv6Nw="fd53:7cb8:383:4::/64"

L2tpArIpv6Addr="fd53:7cb8:383:4::123"
L2tpUbuntuIpv6Addr="fd53:7cb8:383:4::124"
#L2tpUbuntuIpv6Addr="fd53:7cb8:383:4::108"

L2tpUbuntuIpv4Addr="192.168.225.60"
L2tpUbuntuIpv4Nw="255.255.255.0"
L2tpUbuntuIpv4Nw2="192.168.225.0/24"

UbuntuIpv4Addr=""
fUbuntuIpv4AddrGet()
{
	local Itf="$1"

	UbuntuIpv4Addr=$(ip addr show dev $Itf | sed -e's/^.*inet \([^ ]*\)\/.*$/\1/;t;d'); fRetCheck
}

UbuntuIpv6Addr=""
fUbuntuIpv6AddrGet()
{
	local Itf="$1"

	UbuntuIpv6Addr=$(ip addr show dev $Itf | sed -e's/^.*inet6 \([^ ]*\)\/.*$/\1/;t;d'); fRetCheck
}

BridgeArIpv4Addr="192.168.225.1"
BridgeArIpv6Addr=""

fBridgeArIpv6AddrGet()
{
	BridgeArIpv6Addr=$(adb shell "source /etc/profile; ip addr show dev bridge0 | sed -e's/^.*inet6 \([^ ]*\)\/.*$/\1/;t;d'"); fRetCheck
	BridgeArIpv6Addr=${BridgeArIpv6Addr%$'\r'}
}

ChinaUnicomGuangdongIpv4Addr1="163.177.151.108"
ChinaUnicomGuangdongIpv4Addr2="163.177.151.109"
ChinaUnicomGuangdongIpv6Addr="2400:da00:2::29"
ChinaTelecomGuangdongIpv6Addr="240e:1f:1::1"

ip_Port_Igmp=2
ip_Port_FtpData=20
ip_Port_FtpCtrl=21
ip_Port_Ssh=22
ip_Port_Http=80
ip_Port_L2tp=115
ip_Port_Ntp=123
ip_Port_HttpSec=443
ip_Port_SocksProxy=1080
ip_Port_Iperf=5201

IpPerfsProto="tcp"
#IpPerfsProto="udp"

ArUsbDmDev="/dev/ttyUSB0"
#ArUsbDmDev="/dev/ttyUSB1"

# Using a SWI AR official release, w/o any customization ?
ArBuildOfficial="n"

#DmCaptDir="$HOME/Documents/SLQS/SLQS04.00.23/tools/logging/dm/"
DmCaptDir="$HOME/Documents/SLQS/SLQS04.00.25.src/tools/logging/dm"

fKeyToContinue()
{
	read -p "Press [Enter] to continue ..."
}

fRemoteCmd()
{
	local Cmd="$1"

	adb shell "source /etc/profile; $Cmd"; fRetCheck
}
fRemoteCmdAt()
{
	local CmdAt="$1"
	local Cmd

	Cmd=$(printf "chat -Vs '' '%s' 'OK' '' > /dev/ttyAT < /dev/ttyAT" "$CmdAt")
	fRemoteCmd "$Cmd"
}

fRemotePhyItfWait()
{
	local Itf="$1"

	while true; do
		adb shell "source /etc/profile; ip l show $Itf" | grep "DOWN"
		if [[ $? -eq 0 ]]; then
			printf "${DisplWrn}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Itf"
			fRemoteCmd "ip l show $Itf"
			sleep 1
		else
			printf "${DisplOk}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Itf"
			fRemoteCmd "ip l show $Itf"
			break
		fi
	done
}
fRemotePhyItfWaitDown()
{
	local Itf="$1"

	while true; do
		adb shell "source /etc/profile; ip l show $Itf" | grep "does\ not\ exist"
		if [[ $? -eq 0 ]]; then
			printf "${DisplOk}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Itf"
			fRemoteCmd "ip l show $Itf"
			break
		else
			printf "${DisplWrn}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Itf"
			fRemoteCmd "ip l show $Itf"
			sleep 1
		fi
	done
}

fRemoteItfWait()
{
	local Itf="$1"

	while true; do
		adb shell "source /etc/profile; ip a show $Itf" | grep "DOWN"
		if [[ $? -eq 0 ]]; then
			printf "${DisplWrn}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Itf"
			fRemoteCmd "ip a show $Itf"
			sleep 1
		else
			printf "${DisplOk}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Itf"
			fRemoteCmd "ip a show $Itf"
			break
		fi
	done
}

fLocalPhyItfWait()
{
	local Itf="$1"

	while true; do
		ip l show $Itf | grep "DOWN"
		if [[ $? -eq 0 ]]; then
			printf "${DisplWrn}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Itf"
			ip l show $Itf
			sleep 1
		else
			printf "${DisplOk}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Itf"
			ip l show $Itf
			break
		fi
	done
}

fLocalItfWait()
{
	local Itf="$1"

	while true; do
		ip a show $Itf
		if [[ $? -ne 0 ]]; then
			printf "${DisplWrn}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Itf"
			ip a show $Itf
			sleep 1
		else
			printf "${DisplOk}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Itf"
			ip a show $Itf
			break
		fi
	done
}

fLocalFwallLog()
{
	local IpTblCmd="$1"
	local Drop="$2"

	sudo $IpTblCmd -N LOGGING
	sudo $IpTblCmd -A OUTPUT -j LOGGING
	sudo $IpTblCmd -A INPUT -j LOGGING
	sudo $IpTblCmd -A FORWARD -j LOGGING
	if [[ "$Drop" == "y" ]]; then
		sudo $IpTblCmd -A LOGGING -m limit --limit 60/min -j LOG --log-prefix "$IpTblCmd-LoggedNDropped: " --log-level 4
		sudo $IpTblCmd -A LOGGING -j DROP
	else
		sudo $IpTblCmd -A LOGGING -m limit --limit 60/min -j LOG --log-prefix "$IpTblCmd-Dropped: " --log-level 4
	fi

	#sudo $IpTblCmd -S | grep LOGGING
}

fRemoteFwallReset()
{
	if [[ "$ArBuildOfficial" == "n" ]]; then
		tFilterRulesIpv4=( \
		"-A INPUT -i lo -j ACCEPT" \
		"-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT" \
		"-A INPUT -p udp -m udp --sport 53 -j ACCEPT" \
		"-A INPUT -p icmp -m icmp --icmp-type 0 -j ACCEPT" \
		"-A INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT" \
		"-A INPUT -i eth0 -p tcp -m tcp --dport 22 -j ACCEPT" \
		"-A INPUT -i ecm0 -p tcp -m tcp --dport 22 -j ACCEPT" \
		"-A INPUT -i bridge0 -p tcp -m tcp --dport 22 -j ACCEPT" \
		"-A INPUT -i bridge0 -p udp -m udp --dport 67 -j ACCEPT" \
		"-A INPUT -i bridge0 -p udp -m udp --sport 68 -j ACCEPT" \
		"-A INPUT -p udp -m udp --dport 53 -j ACCEPT" \
		"-A INPUT -p tcp -m tcp --dport 53 -j ACCEPT" \
		"-A FORWARD -p icmp -m icmp --icmp-type 0 -j ACCEPT" \
		"-A FORWARD -p icmp -m icmp --icmp-type 8 -j ACCEPT" \
		"-A FORWARD -p tcp -m tcp --dport 443 -j ACCEPT" \
		"-A FORWARD -p tcp -m tcp --sport 443 -j ACCEPT" \
		"-A FORWARD -p tcp -m tcp --dport 80 -j ACCEPT" \
		"-A FORWARD -p tcp -m tcp --sport 80 -j ACCEPT" \
		)
		tFilterRulesIpv6=( \
		"-A INPUT -i lo -j ACCEPT" \
		"-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT" \
		"-A INPUT -p udp -m udp --sport 53 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 1 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 2 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 3 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 4 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 133 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 134 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 135 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 136 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 137 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 141 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 142 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 148 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 149 -j ACCEPT" \
		"-A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 130 -j ACCEPT" \
		"-A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 131 -j ACCEPT" \
		"-A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 132 -j ACCEPT" \
		"-A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 143 -j ACCEPT" \
		"-A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 151 -j ACCEPT" \
		"-A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 152 -j ACCEPT" \
		"-A INPUT -s fe80::/10 -p ipv6-icmp -m icmp6 --icmpv6-type 153 -j ACCEPT" \
		"-A INPUT -i eth0 -p tcp -m tcp --dport 22 -j ACCEPT" \
		"-A INPUT -i ecm0 -p tcp -m tcp --dport 22 -j ACCEPT" \
		"-A INPUT -i bridge0 -p tcp -m tcp --dport 22 -j ACCEPT" \
		"-A INPUT -i bridge0 -p udp -m udp --dport 67 -j ACCEPT" \
		"-A INPUT -i bridge0 -p udp -m udp --sport 68 -j ACCEPT" \
		"-A INPUT -p udp -m udp --dport 53 -j ACCEPT" \
		"-A INPUT -p tcp -m tcp --dport 53 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -j ACCEPT" \
		"-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 129 -j ACCEPT" \
		"-A FORWARD -p tcp -m tcp --dport 443 -j ACCEPT" \
		"-A FORWARD -p tcp -m tcp --sport 443 -j ACCEPT" \
		"-A FORWARD -p tcp -m tcp --dport 80 -j ACCEPT" \
		"-A FORWARD -p tcp -m tcp --sport 80 -j ACCEPT" \
		)

		fRemoteCmd "iptables -F -t nat"
		fRemoteCmd "iptables -F -t filter"
		fRemoteCmd "iptables -X LOGGING"
		fRemoteCmd "ip6tables -F -t nat"
		fRemoteCmd "ip6tables -F -t filter"
		fRemoteCmd "ip6tables -X LOGGING"

		fRemoteCmd "iptables -t nat -A POSTROUTING -o $WwanQcmapItf -j MASQUERADE --random"
		fRemoteCmd "iptables -t nat -A POSTROUTING -o $WwanQcmapItf -j MASQUERADE --random"
		fRemoteCmd "ip6tables -t nat -A POSTROUTING -o $WwanMdcItf -j MASQUERADE --random"
		fRemoteCmd "ip6tables -t nat -A POSTROUTING -o $WwanMdcItf -j MASQUERADE --random"

		for Idx in ${!tFilterRulesIpv4[*]}; do
			Rule=${tFilterRulesIpv4[$Idx]}
			fRemoteCmd "iptables -t filter $Rule"
		done
		for Idx in ${!tFilterRulesIpv6[*]}; do
			Rule=${tFilterRulesIpv6[$Idx]}
			fRemoteCmd "ip6tables -t filter $Rule"
		done
	else
		fRemoteCmd "iptables-restore < /etc/iptables/rules.v4"
		fRemoteCmd "ip6tables-restore < /etc/iptables/rules.v6"
	fi

	fRemoteCmd "ip6tables -A OUTPUT -o bridge0 -p tcp -m tcp --sport 22 -j ACCEPT"
	fRemoteCmd "ip6tables -A OUTPUT -o bridge0 -p tcp -m tcp --sport 67 --dport 68 -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -i bridge0 -p 2 -j ACCEPT"
	fRemoteCmd "iptables -A INPUT -i eth0 -p udp -m udp --sport 68 --dport 67 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p tcp -m tcp --sport 22 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p udp -m udp --sport 53 -j ACCEPT"
	fRemoteCmd "iptables -A OUTPUT -o bridge0 -p udp -m udp --sport 67 --dport 68 -j ACCEPT"
}

fRemoteFwallLog()
{
	local IpTblCmd="$1"
	local Drop="$2"

	fRemoteCmd "$IpTblCmd -N LOGGING"
	fRemoteCmd "$IpTblCmd -A OUTPUT --jump LOGGING"
	fRemoteCmd "$IpTblCmd -A INPUT --jump LOGGING"
	fRemoteCmd "$IpTblCmd -A FORWARD --jump LOGGING"
	if [[ "$Drop" == "y" ]]; then
		fRemoteCmd "$IpTblCmd -A LOGGING -m limit --limit 60/min -j LOG --log-prefix \"$IpTblCmd-LoggedNDropped: \" --log-level 4"
		fRemoteCmd "$IpTblCmd -A LOGGING --jump DROP"
	else
		fRemoteCmd "$IpTblCmd -A LOGGING -m limit --limit 60/min -j LOG --log-prefix \"$IpTblCmd-Logged: \" --log-level 4"
	fi

	#adb shell "source /etc/profile; $IpTblCmd -S | grep LOGGING"
}

fRemoteFwallGet()
{
	tTables=( \
	raw \
	nat \
	mangle \
	filter \
	)

	for Idx in ${!tTables[*]}; do
		Table=${tTables[$Idx]}
		fRemoteCmd "iptables -S -t $Table"
		fKeyToContinue
		fRemoteCmd "ip6tables -S -t $Table"
		fKeyToContinue
	done
}

fRemoteFwallMon()
{
	adb shell "logread -f | grep \"kernel\|iptables\|ip6tables\" | grep -v \"mmc\|sdhci\|unix_release\""
}

fLocalLan()
{
	local Action="$1"
	local PhyItf="$2"

	case "$Action" in
		on)
			;;
		check)
			fLocalPhyItfWait "$PhyItf"
			fLocalItfWait "$PhyItf"
			;;
		off)
			;;
	esac
}

fLocalDnsGet()
{
	local Itf="$1"

	sudo systemd-resolve --set-dns="$DnsSrvArIpv4Addr" --interface="$Itf"
	#cat /etc/systemd/resolved.conf
	#cat /etc/resolv.conf
	#fKeyToContinue

	systemd-resolve --status
	#systemd-resolve --statistics
}

fRemoteDnsGet()
{
	fRemoteCmd "cat /etc/resolv.conf"


	fRemoteCmd "cm data profile $WwanQcmapProfileId"

	Tmp1=$(adb shell "source /etc/profile; cm data info" | grep "Dns1\[IPv4\]\:")
	Tmp2=( $Tmp1 )
	DnsSrvQcmapIpv4Addr="${Tmp2[1]}"

	Tmp1=$(adb shell "source /etc/profile; cm data info" | grep "Dns1\[IPv6\]\:")
	Tmp2=( $Tmp1 )
	DnsSrvQcmapIpv6Addr="${Tmp2[1]}"

	fKeyToContinue


	fRemoteCmd "cm data profile $WwanQcmapProfileId"

	Tmp1=$(adb shell "source /etc/profile; cm data info" | grep "Dns1\[IPv4\]\:")
	Tmp2=( $Tmp1 )
	DnsSrvMdcIpv4Addr="${Tmp2[1]}"

	Tmp1=$(adb shell "source /etc/profile; cm data info" | grep "Dns1\[IPv6\]\:")
	Tmp2=( $Tmp1 )
	DnsSrvMdcIpv6Addr="${Tmp2[1]}"

	fKeyToContinue
}

fLocalIpFwd()
{
	#sudo cat /proc/sys/net/ipv4/ip_forward
	sudo sysctl net.ipv4.ip_forward
	sudo sysctl -w net.ipv4.ip_forward=1
}

fLocalRpFilter()
{
	local PhyItf="$1"
	local Action="$2"

	case "$Action" in
		on)
			sudo sysctl net.ipv4.conf.all.rp_filter=1
			sudo sysctl net.ipv4.conf.$PhyItf.rp_filter=2
			;;
		off)
			sudo sysctl net.ipv4.conf.all.rp_filter=0
			sudo sysctl net.ipv4.conf.$PhyItf.rp_filter=0
			;;
	esac
}

fRemoteConnectivityPrepare()
{
	fRemoteCmd "cm sim enterpin 1234"

	fRemoteCmd "cm data profile $WwanQcmapProfileId"
	fRemoteCmd "cm data apn orange"
	fRemoteCmd "cm data auth none orange orange"
	#fRemoteCmd "cm data pdp IPV4V6"
	#fRemoteCmd "cm data map $WwanQcmapItf"

	fRemoteCmd "cm data profile $WwanMdcProfileId"
	fRemoteCmd "cm data apn orange.fr"
	fRemoteCmd "cm data auth none orange orange"
	#fRemoteCmd "cm data pdp IPV4V6"
	fRemoteCmd "cm data map $WwanMdcItf"

	fRemoteCmd "echo 0 > /proc/sys/net/ipv4/conf/all/rp_filter"
	fRemoteCmd "echo 0 > /proc/sys/net/ipv4/conf/$PhyItf/rp_filter"
}

fRemoteCmdSwiQcmap()
{
	local Cmd="$1"
	local Rsp

	Rsp=$(adb shell "source /etc/profile; swi_qcmap_cli $Cmd")
	if [[ "$Rsp" == *"successfully"* ]]; then
		printf "${DisplOk}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "OK"
	elif [[ "$Rsp" == *"backhaul runs ok"* ]]; then
		printf "${DisplOk}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "OK"
	else
		printf "${DisplErr}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Rsp"
		fKeyToContinue
	fi
}

fRemoteBackhaulWait()
{
	local State="$1"

	fRemoteCmd "cm radio"

	while true; do
		adb shell "source /etc/profile; swi_qcmap_cli backhaul get" | grep BackhaulState:$State
		if [[ $? -ne 0 ]]; then
			printf "${DisplWrn}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$State"
			fRemoteCmdSwiQcmap "backhaul get"
			sleep 1
		else
			printf "${DisplOk}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$State"
			break
		fi
	done
}
fRemoteMdcWait()
{
	local State="$1"

	fRemoteCmd "cm radio"
	fRemoteCmd "cm data info"

	while true; do
		adb shell "source /etc/profile; cm data" | grep Connected | grep $State
		if [[ $? -ne 0 ]]; then
			printf "${DisplWrn}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$State"
			fRemoteCmd "cm data"
			sleep 1
		else
			printf "${DisplOk}%s %s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$State"
			break
		fi
	done
}

fRemoteIpCheck()
{
	local Itf="$1"
	local IpVer="$2"
	local Ret

	Rsp=$(adb shell "source /etc/profile; ip -$IpVer a show $Itf")
	RspFiltered=$(echo "$Rsp" | grep global)
	if [[ $? -eq 0 ]]; then
		printf "${DisplOk}%s\n%s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$RspFiltered"
	else
		printf "${DisplErr}%s\n%s${DisplNormal}\n" "$FUNCNAME:$LINENO" "$Rsp"
		fKeyToContinue
	fi
}

fRemoteLan()
{
	local Action="$1"
	local PhyItf="$2"

	case "$Action" in
		on)
			fRemoteCmd "modprobe DWC_ETH_QOS.ko fw_filename=DWC_ETH_QOS_fw.bin"
			;;
		check)
			fRemotePhyItfWait "$PhyItf"
			fRemoteItfWait "$PhyItf"
			;;
		off)
			fRemoteCmd "modprobe -r DWC_ETH_QOS.ko"
			fRemotePhyItfWaitDown "$PhyItf"
			;;
	esac
}

fRemoteWwan()
{
	local Action="$1"
	local Opt="$2"

	if [ -z "$Opt" ]; then
		Opt="qcmap"
	fi

	case "$Action" in
		on)
			case "$Opt" in
				qcmap)
					fRemoteCmdSwiQcmap "backhaul config autoConnect $AutoConnect roaming 1 ProfileID $WwanQcmapProfileId"
					fRemoteCmdSwiQcmap "backhaul get"
					fRemoteCmdSwiQcmap "backhaul set state 1"
					;;
				mdc)
					fRemoteCmd "cm data profile $WwanMdcProfileId"
					fRemoteCmd "cm data info"
					fRemoteCmd "cm data connect"
					;;
			esac
			;;
		check)
			case "$Opt" in
				qcmap)
					fRemoteBackhaulWait "1"

					fRemotePhyItfWait "$WwanQcmapItf"
					fRemoteItfWait "$WwanQcmapItf"

					fRemoteIpCheck "$WwanQcmapItf" "4"
					fRemoteIpCheck "$WwanQcmapItf" "6"
					;;
				mdc)
					fRemoteMdcWait "yes"

					fRemotePhyItfWait "$WwanMdcItf"
					fRemoteItfWait "$WwanMdcItf"

					fRemoteIpCheck "$WwanMdcItf" "4"
					fRemoteIpCheck "$WwanMdcItf" "6"
					;;
			esac

			fRemoteCmd "ip route"
			fRemoteCmd "ip rule"

			fRemoteDnsGet
			;;
		status)
			case "$Opt" in
				qcmap)
					fRemoteCmdSwiQcmap "backhaul get"
					;;
				mdc)
					fRemoteCmd "cm data info"
					;;
			esac
			;;
		off)
			fRemoteCmd "ip route"
			fRemoteCmd "ip rule"

			case "$Opt" in
				qcmap)
					fRemoteCmdSwiQcmap "backhaul set state 0"
					fRemoteCmdSwiQcmap "backhaul config autoConnect 0 roaming 0 ProfileID $WwanQcmapProfileId"

					fRemoteBackhaulWait "0"
					;;
				mdc)
					fRemoteCmd "cm data profile $WwanMdcProfileId"
					fRemoteCmd "cm data info"
					fRemoteCmd "cm data disconnect"

					fRemoteMdcWait "no"
					;;
			esac
			;;
	esac
}

fRemoteIpaStatsGet()
{
	local Log

	fRemoteCmd "rm /tmp/*.ipastats.log"

	Log=$(printf "/tmp/")
	Log+=$(printf "%s" "$Stamp")
	Log+=$(printf "%s" ".ipastats.log")

	fRemoteCmd "touch $Log"

	tStats=( \
	"/sys/kernel/debug/ipa/active_clients" \
	"/sys/kernel/debug/ipa/clock_scaling_bw_threshold_nominal_mbps" \
	"/sys/kernel/debug/ipa/clock_scaling_bw_threshold_turbo_mbps" \
	"/sys/kernel/debug/ipa/dbg_cnt" \
	"/sys/kernel/debug/ipa/enable_clock_scaling" \
	"/sys/kernel/debug/ipa/enable_low_prio_print" \
	"/sys/kernel/debug/ipa/ep_reg" \
	"/sys/kernel/debug/ipa/gen_reg" \
	"/sys/kernel/debug/ipa/hdr" \
	"/sys/kernel/debug/ipa/holb" \
	"/sys/kernel/debug/ipa/hw_type" \
	"/sys/kernel/debug/ipa/ip4_flt" \
	"/sys/kernel/debug/ipa/ip4_nat" \
	"/sys/kernel/debug/ipa/ip4_rt" \
	"/sys/kernel/debug/ipa/ip6_flt" \
	"/sys/kernel/debug/ipa/ip6_rt" \
	"/sys/kernel/debug/ipa/ipa_poll_iteration" \
	"/sys/kernel/debug/ipa/ipa_rx_poll_time" \
	"/sys/kernel/debug/ipa/keep_awake" \
	"/sys/kernel/debug/ipa/msg" \
	"/sys/kernel/debug/ipa/ntn" \
	"/sys/kernel/debug/ipa/proc_ctx" \
	"/sys/kernel/debug/ipa/rm_stats" \
	"/sys/kernel/debug/ipa/stats" \
	#"/sys/kernel/debug/ipa/status_stats" \
	"/sys/kernel/debug/ipa/wdi" \
	"/sys/kernel/debug/ipa/wstats" \
	)

	for Idx in ${!tStats[*]}; do
		Stat=${tStats[$Idx]}
		fRemoteCmd "echo $Stat >> $Log"
		fRemoteCmd "cat $Stat >> $Log"
		fRemoteCmd "echo ---- >> $Log"
	done

	adb pull "$Log" "./logs/"
}

fIcmpExt()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local Cmd

	Cmd=$(printf "ping -%s -c 2 -d -W 15" "$IpVer")
	if [ ! -z "$Itf" ]; then
		Cmd+=$(printf " -I %s" "$Itf")
	fi
	Cmd+=$(printf " %s" "$Host")

	$Cmd; fRetCheck "$?"

	#host -v "$IpVer" "$1"; fRetCheck "$?"

	#nslookup "$Host"; fRetCheck "$?"
}

fIcmpRemoteExt()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local Cmd
	local Rsp
	local Ret="$?"

	Cmd=$(printf "ping -%s -c 2 -W 15" "$IpVer")
	if [ ! -z "$Itf" ]; then
		Cmd+=$(printf " -I %s" "$Itf")
	fi
	Cmd+=$(printf " %s" "$Host")

	Rsp=$(adb shell "source /etc/profile; $Cmd")
	Ret="$?"
	case $Ret in
		0)
			if [[ "$Rsp" == *"unreachable"* ]]; then
				printf "${DisplErr}%s${DisplNormal}\n" "$Rsp"
				fKeyToContinue
			else
				printf "${DisplOk}OK${DisplNormal}\n"
			fi
			;;
		*)
			printf "${DisplErr}NOK %s${DisplNormal}\n" "$Ret"
			fKeyToContinue
			;;
	esac
}

fCurl()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local Opt="$4"
	local Cmd

	Cmd=$(printf "sudo curl -v -%s --connect-timeout 30 --max-time 60" "$IpVer")
	if [ ! -z "$Itf" ]; then
		Cmd+=$(printf " --interface %s" "$Itf")
	fi
	if [ ! -z "$Opt" ]; then
		Cmd+=$(printf " %s" "$Opt")
	fi
	Cmd+=$(printf " %s" "$Host")

	$Cmd; fRetCheck "$?"
}

fCurlFtpDlExt()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local Opt

	Opt=$(printf "%s %s" "--output /dev/null --ftp-port" "$Itf")

	fCurl "$IpVer" "$Host" "$Itf" "$Opt"
}
fCurlFtpUlExt()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local FileUl
	local FileUlSize
	local Opt

	FileUlSize="1K"
	#FileUlSize="1M"
	#FileUlSize="1G"
	FileUl=$(printf "/tmp/test_%s.bin" "$FileUlSize")
	fallocate -v --length "$FileUlSize" "$FileUl"

	#-P filecontent=@$FileUl
	#-P file=@$FileUl

	Opt=$(printf "%s %s" "--ftp-port" "$Itf")

	fCurl "$IpVer" "$Host" "$Itf" "$Opt"

	rm -i "$FileUl"
}

fCurlHttpDlExt()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local Opt="$4"

	Opt+=$(printf "%s" " --output /dev/null")

	fCurl "$IpVer" "$Host" "$Itf" "$Opt"
}
fCurlHttpUlExt()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local FileUl
	local FileUlSize
	local Opt

	# https://ptsv2.com/

	FileUlSize="1K"
	#FileUlSize="50M"
	#FileUlSize="1G"
	FileUl=$(printf "/tmp/test_%s.bin" "$FileUlSize")
	fallocate -v --length "$FileUlSize" "$FileUl"

	Opt=$(printf "%s %s" "--upload-file" "$FileUl")

	fCurl "$IpVer" "$Host" "$Itf" "$Opt"

	rm -i "$FileUl"
}

fCurlRemote()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local Opt="$4"
	local Cmd

	Cmd=$(printf "curl -v -%s --connect-timeout 30 --max-time 60" "$IpVer")
	if [ ! -z "$Itf" ]; then
		Cmd+=$(printf " --interface %s" "$Itf")
	fi
	if [ ! -z "$Opt" ]; then
		Cmd+=$(printf " %s" "$Opt")
	fi
	Cmd+=$(printf " %s" "$Host")

	Rsp=$(adb shell "source /etc/profile; $Cmd")
	Ret="$?"
	case $Ret in
		0|28)
			if [[ "$Rsp" == *"unreachable"* ]]; then
				printf "${DisplErr}%s${DisplNormal}\n" "$Rsp"
				fKeyToContinue
			else
				printf "${DisplOk}OK${DisplNormal}\n"
			fi
			;;
		*)
			printf "${DisplErr}NOK %s${DisplNormal}\n" "$Ret"
			fKeyToContinue
			;;
	esac
}

fCurlRemoteFtpDlExt()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local Opt

	Opt=$(printf "%s %s" "--output /dev/null --ftp-port" "$Itf")

	fCurlRemote "$IpVer" "$Host" "$Itf" "$Opt"
}
fCurlRemoteFtpUlExt()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local FileUl
	local FileUlSize
	local Opt
	local Cmd

	FileUlSize="1K"
	#FileUlSize="1M"
	#FileUlSize="1G"
	FileUl=$(printf "/tmp/test_%s.bin" "$FileUlSize")
	Cmd=$(printf "dd if=/dev/zero of=\"%s\" bs=%s count=0 seek=%s" "$FileUl" "$FileUlSize" "$FileUlSize")
	fRemoteCmd "$Cmd"

	#-P filecontent=@$FileUl
	#-P file=@$FileUl

	Opt=$(printf "%s %s" "--ftp-port" "$Itf")
	fCurlRemote "$IpVer" "$Host" "$Itf" "$Opt"

	Cmd=$(printf "rm -i \"%s\"" "$FileUl")
	fRemoteCmd "$Cmd"
}

fCurlRemoteHttpDlExt()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local Opt="$4"

	Opt+=$(printf "%s" " --output /dev/null")
	fCurlRemote "$IpVer" "$Host" "$Itf" "$Opt"
}
fCurlRemoteHttpUlExt()
{
	local IpVer="$1"
	local Host="$2"
	local Itf="$3"
	local FileUl
	local FileUlSize
	local Opt
	local Cmd

	# https://ptsv2.com/

	FileUlSize="1K"
	#FileUlSize="50M"
	#FileUlSize="1G"
	FileUl=$(printf "/tmp/test_%s.bin" "$FileUlSize")
	Cmd=$(printf "dd if=/dev/zero of=\"%s\" bs=%s count=0 seek=%s" "$FileUl" "$FileUlSize" "$FileUlSize")
	fRemoteCmd "$Cmd"

	Opt=$(printf "%s %s" "--upload-file" "$FileUl")
	fCurlRemote "$IpVer" "$Host" "$Itf" "$Opt"

	Cmd=$(printf "rm -i \"%s\"" "$FileUl")
	fRemoteCmd "$Cmd"
}

fDhcpRenew()
{
	local Itf="$1"

	sudo dhclient -r -v "$Itf"
	sudo dhclient -v "$Itf"
}

fEnvGetLocal()
{
	echo "$Stamp"

	lsb_release -a
	uname -a

	lsusb
	lsusb -t
	ls /dev/ttyUSB*

	#lshw -class network

	curl --version
	iperf --version
	iperf3 --version

	cat /proc/sys/net/core/rmem_max
	cat /proc/sys/net/core/wmem_max
	cat /proc/sys/net/core/rmem_default
	cat /proc/sys/net/core/wmem_default
	cat /proc/sys/net/ipv4/tcp_rmem
	cat /proc/sys/net/ipv4/tcp_wmem
	cat /proc/sys/net/ipv4/tcp_window_scaling
}

fEnvGetRemote()
{
	adb devices

	fRemoteCmd "cat /etc/legato/version"
	fRemoteCmd "uname -a"
	fRemoteCmd "modprobe -lv"
	fRemoteCmd "lsmod -v"
	fRemoteCmd "cm info"
	fRemoteCmd "cm data info"
	fRemoteCmd "cm sim info"
	fRemoteCmd "swi_qcmap_cli -Version"
	fRemoteCmd "cat /sys/kernel/debug/ipa/hw_type"

	#fRemoteCmd "curl --version"
	#fRemoteCmd "iperf --version"
	#fRemoteCmd "iperf3 --version"
	#fRemoteCmd "tcpdump --version"

	if false; then
		# chat package missing inside Linux image
		fRemoteCmdAt "AT"
		fRemoteCmdAt "ATI8"
		fRemoteCmdAt "AT!SELRAT?"
		fRemoteCmdAt "AT+CGDCONT?"
	fi

	fRemoteCmd "cat /proc/sys/net/core/rmem_max"
	fRemoteCmd "cat /proc/sys/net/core/wmem_max"
	fRemoteCmd "cat /proc/sys/net/core/rmem_default"
	fRemoteCmd "cat /proc/sys/net/core/wmem_default"
	fRemoteCmd "cat /proc/sys/net/ipv4/tcp_rmem"
	fRemoteCmd "cat /proc/sys/net/ipv4/tcp_wmem"
	fRemoteCmd "cat /proc/sys/net/ipv4/tcp_window_scaling"
}
